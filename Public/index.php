<?php
ob_start();
ini_set('session.gc_maxlifetime', 3600 * 24 * 31);
session_set_cookie_params(3600 * 24 * 31);

error_reporting(E_ALL | E_WARNING | E_NOTICE);
ini_set('display_errors', TRUE);

defined('APP_PATH') || define('APP_PATH', realpath(dirname(__FILE__) . '/../App'));
defined('DIR_VENDOR') || define('DIR_VENDOR', realpath(dirname(__FILE__) . '/../vendor'));
defined('DIR_UPLOAD') || define('DIR_UPLOAD', realpath(dirname(__FILE__) . '/../Data/upload'));

try {
    require '../config.php';
    require DIR_VENDOR . '/autoload.php';

    $settings = require APP_PATH . '/Config/settings.php';
    $app = new \App\Engine\App($settings);

    require APP_PATH . '/Config/dependencies.php';
    require APP_PATH . '/Config/routes.php';

    $app->run();

} catch (\Throwable $error) {
    http_response_code(500);
    if (isset($settings['settings']['displayErrorDetails']) && $settings['settings']['displayErrorDetails'] === true) {
        echo '<html><body>
                <b>', get_class($error), '</b>: ', $error->getMessage(), '<br>
                <br>', $error->getFile(), ' on line <b>', $error->getLine(), '</b>
                <pre>', $error->getTraceAsString(), '</pre>
                </body></html>';
    } else {
        ob_clean();
        echo '<html><body><h1>500 Internal Server Error</h1></body></html>';
    }
}
