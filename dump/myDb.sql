-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: pixel
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event_calls`
--

DROP TABLE IF EXISTS `event_calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_brand` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_model` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_os` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_version` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `event_calls_events_id_fk` (`event_id`),
  CONSTRAINT `event_calls_events_id_fk` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_calls`
--

LOCK TABLES `event_calls` WRITE;
/*!40000 ALTER TABLE `event_calls` DISABLE KEYS */;
INSERT INTO `event_calls` VALUES (1,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-05 09:34:23'),(2,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-05 09:35:44'),(3,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-07 20:42:15'),(4,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 19:48:37'),(5,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 19:48:39'),(6,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 19:48:42'),(7,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 19:48:43'),(8,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 19:48:46'),(9,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 19:48:48'),(10,4,'172.19.0.1','pl-PL','Samsung','smartphone','Android ','Chrome Mobile','71.0','2019-01-08 19:59:40'),(11,4,'172.19.0.1','pl-PL','Samsung','smartphone','Android ','Chrome Mobile','71.0','2019-01-08 20:00:09'),(12,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 20:01:03'),(13,4,'172.19.0.1','pl-pl','Apple','desktop','Mac ','Safari','12.0','2019-01-08 20:01:09'),(14,4,'172.19.0.1','pl-PL','Samsung','smartphone','Android ','Chrome Mobile','71.0','2019-01-08 20:10:10'),(15,8,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 22:29:12'),(16,8,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 22:29:51'),(17,8,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 22:29:58'),(18,8,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 22:30:17'),(19,3,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 22:56:47'),(20,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:37:05'),(21,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:37:08'),(22,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:37:10'),(23,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:37:13'),(24,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:37:15'),(25,4,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:37:18'),(26,4,'172.19.0.1','pl-PL','Apple','tablet','iOS ','Firefox Mobile','iOS 1.0','2019-01-08 23:50:32'),(27,3,'172.19.0.1','pl','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:55:24'),(28,3,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:55:51'),(29,3,'172.19.0.1','pl-PL','RIM','smartphone','BlackBerry OS ','BlackBerry Browser','','2019-01-08 23:56:29'),(30,3,'172.19.0.1','pl-PL','','desktop','Windows x64','Microsoft Edge','12.10240','2019-01-08 23:56:56'),(31,8,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-08 23:57:56'),(35,17,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-10 20:01:32'),(36,18,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-10 20:15:21'),(37,22,'172.19.0.1','pl-PL','Apple','desktop','Mac ','Chrome','71.0','2019-01-10 20:20:09');
/*!40000 ALTER TABLE `event_calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `events_hash_uindex` (`hash`),
  KEY `events_pages_id_fk` (`page_id`),
  KEY `events_page_id_name_index` (`page_id`,`name`),
  CONSTRAINT `events_pages_id_fk` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (2,3,'Główna','12','2019-01-03 23:50:29','2019-01-04 00:09:33'),(3,3,'Nowy','asd','2019-01-04 00:06:43',NULL),(4,3,'Abc','2B6prvNkLdl8ay1UqNsc','2019-01-04 00:23:43',NULL),(8,3,'Głównaasdasd','VoaNxBXeUrGmLGhjOMLj','2019-01-08 21:55:36',NULL),(9,3,'Abcasas','4nDdeVN092oYCyoW5DcG','2019-01-08 22:03:54','2019-01-10 19:33:23'),(17,3,'asdasd','2HRwl3CimtewaWQiO8aE','2019-01-10 20:01:16',NULL),(18,6,'Mario','Cta7SWsidh3NoLHNhHNv','2019-01-10 20:14:55',NULL),(22,7,'Nowy','whryhVipkxYFPFDkwexE','2019-01-10 20:19:57',NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_users_id_fk` (`owner_id`),
  CONSTRAINT `pages_users_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'localhost','localhost',1,'2019-01-03 21:05:04','2019-01-03 21:31:27'),(2,'asn','asn',1,'2019-01-03 21:05:04','2019-01-03 21:31:45'),(3,'http://localhost:8000/','localhost',1,'2019-01-03 21:05:04',NULL),(4,'https://www.wp.pl/','Testowa',1,'2019-01-03 23:03:50',NULL),(5,'http://a.pl','asd',1,'2019-01-10 20:10:25','2019-01-10 20:10:30'),(6,'http://a.pl','112',1,'2019-01-10 20:13:27',NULL),(7,'http://Wakacje.pl','Moja nowa strona',4,'2019-01-10 20:17:43',NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_users_id_fk` (`created_by`),
  CONSTRAINT `users_users_id_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tomasz@toperko.pl','$2y$10$SGs5fBkXzeR.UMDo4Hj5ruAafNya3VBINhRMtTDpHqfXQZVQdPMxq',100,NULL,'2018-12-08 13:10:09','2019-01-11 22:11:19'),(4,'nowy2@User.pl','$2y$10$jD96kDTbiXPajYe4xmuRCuxDclRNjkpulycAQzM9kA2FV9RgUZxH2',10,1,'2019-01-10 20:16:21','2019-01-10 20:17:06');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-11 23:16:26
