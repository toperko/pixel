$(document).ready(function () {
    $.ajax({
        dataType: "json",
        url: $(location).attr('href') + '/calls',
        success: function (data) {
            generateTable(data.data);
            generatePagination(data.pageMax, data.pageCurrent);
        }
    });
});

function generateTable(data) {
    $("#event-calls").append("<table class='table'>" +
        "<tr><th>Id</th><th>Ip</th><th>Lang</th><th>Device brand</th><th>Device model</th><th>Device os</th>" +
        "<th>client name</th><th>client version</th><th>created_at</th></tr></table>");
    $.each(data, function (i, item) {
        generateRow(item);
    });
    $("#event-calls img").hide();
}

function generateRow(row) {
    let $tr = $('<tr>').append(
        $('<td>').text(row.id),
        $('<td>').text(row.ip),
        $('<td>').text(row.lang),
        $('<td>').text(row.deviceBrand),
        $('<td>').text(row.deviceModel),
        $('<td>').text(row.deviceOs),
        $('<td>').text(row.clientName),
        $('<td>').text(row.clientVersion),
        $('<td>').text(row.createdAt),
    );
    $("#event-calls table").append($tr);
}

function generatePagination(max, current) {
    $("#event-calls").append($('<nav>'));
    $("#event-calls nav").append('<ul class="pagination"></ul>');
    for (let i = 1; i <= max; i++) {
        let $li = $('<li>');
        $li.addClass('page-item');
        if (i == current) {
            $li.addClass('active');
        }

        let $a = $('<a>');
        $a.text(i);
        $a.attr('href', '#');
        $a.attr('data', i);
        $a.addClass('page-link');
        $li.append($a);

        $("#event-calls ul").append(
            $li
        );
    }

    $(".page-link").click(function (event) {
        event.preventDefault();

        $("#event-calls table").remove();
        $("#event-calls nav").remove();
        $("#event-calls img").show();

        $.ajax({
            dataType: "json",
            url: $(location).attr('href') + '/calls/' + $(this).attr('data'),
            success: function (data) {
                generateTable(data.data);
                generatePagination(data.pageMax, data.pageCurrent);
            }
        });
    });
}