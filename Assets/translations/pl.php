<?php
return [
    'langName'     => 'Polski',
    'emailAddress' => 'Adres email',
    'password'     => 'Hasło',
    'pleaseSingIn' => 'Zaloguj się',
    'rememberMe'   => 'Zampamiętaj mnie',
    'singIn'       => 'Zaloguj',
    'signOut'      => 'Wyloguj',
    'next'         => 'dalej',
    'changeTheme'  => 'Zmień wygląd',
];
