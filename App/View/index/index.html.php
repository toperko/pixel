<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
        if($this->get('lastEventCall')):
    ?>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Last event</h6>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th>Page name:</th>
                    <td><?= $this->get('lastPage')->getName(); ?></td>
                    <th>Event name:</th>
                    <td><?= $this->get('lastEvent')->getName(); ?></td>
                    <th>Time:</th>
                    <td><?= $this->get('lastEventCall')->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
                </tr>
                <tr>
                    <th>Ip:</th>
                    <td><?= $this->get('lastEventCall')->getIp(); ?></td>
                    <th>lang:</th>
                    <td colspan="3"><?= $this->get('lastEventCall')->getLang(); ?></td>
                </tr>
                <tr>
                    <th>Device brand:</th>
                    <td><?= $this->get('lastEventCall')->getDeviceBrand(); ?></td>
                    <th>Device model:</th>
                    <td><?= $this->get('lastEventCall')->getDeviceModel(); ?></td>
                    <th>Device os:</th>
                    <td><?= $this->get('lastEventCall')->getDeviceOs(); ?></td>
                </tr>
                <tr>
                    <th>Client name:</th>
                    <td><?= $this->get('lastEventCall')->getClientName(); ?></td>
                    <th>Client Version:</th>
                    <td colspan="3"><?= $this->get('lastEventCall')->getClientVersion(); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php
    endif;
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    $this->renderHTML('pagesTable', 'pages/', false);
    ?>
</main>