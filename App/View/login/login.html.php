<?php $this->renderHTML("head", 'base/', false); ?>
<body class="text-center">
<form class="form-signin" action="<?= $this->generateUrl('login') ?>" method="post">
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    ?>
    <img class="mb-4" src="<?= $this->getAssetsUrl('img/svg/bar-chart.svg') ?>" alt="Pixel" width="72" height="72">
    <h1 class="h3 mb-3 font-weight-normal"><?= $this->t('pleaseSingIn') ?></h1>
    <label for="inputEmail" class="sr-only"><?= $this->t('emailAddress') ?></label>
    <input type="email" id="inputEmail" class="form-control" placeholder="<?= $this->t('emailAddress') ?>" name="email"
           required
           autofocus>
    <label for="inputPassword" class="sr-only"><?= $this->t('password') ?></label>
    <input type="password" id="inputPassword" class="form-control" placeholder="<?= $this->t('password') ?>"
           name="password" required>
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="rememberMe" name="rememberMe"> <?= $this->t('rememberMe') ?>
        </label>
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit"><?= $this->t('singIn') ?></button>
    <hr>
    <?php
    foreach ($this->translation->getInstalledLang() as $lang) {
        echo sprintf('<a href="%s" class="btn btn-secondary">%s</a> ', $this->generateUrl('changeLang', ['lang' => $lang]), $lang);
    }
    ?>
    <a href="<?= $this->generateUrl('changeTheme') ?>" class="btn btn-secondary"><?= $this->t('changeTheme') ?></a>
    <p class="mt-5 mb-3 text-muted">&copy; <?= date('Y') ?></p>
</form>
</body>
</html>
