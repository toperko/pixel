<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    ?>
    <form action="<?= $this->generateUrl('eventsAddNew') ?>" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="inputPage" class="col-sm-2 col-form-label">Page</label>
            <div class="col-sm-10">
                <select id="inputPage" class="form-control" name="pageId">
                    <option value="0"></option>
                    <?php
                    /**
                     * @var $pages \App\Src\Page\PageModelCollection
                     */
                    $pages = $this->get('pages');
                    foreach ($pages as $key => $page) {
                        echo '<option 
                                value="' . $page->getId() . '" 
                                ' . (($this->get('pageId') == $page->getId()) ? 'selected' : '') . '
                               >' . $page->getName() . '</option>';
                    }
                    ?>

                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEventName" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input
                        id="inputEventName"
                        type="text"
                        class="form-control"
                        name="eventName"
                        placeholder="Name"
                    <?= $this->get('eventName') ? 'value="' . $this->get('eventName') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEventName" class="col-sm-2 col-form-label">File (optional .png)</label>
            <div class="col-sm-10">
                <div class="form-group">
                    <input type="file" name="img" class="file" accept=".png">
                    <div class="input-group col-xs-12">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                        <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                        <span class="input-group-btn">
                            <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </div>
    </form>
</main>