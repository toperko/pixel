<div class="my-3 p-3 box rounded box-shadow">
    <h6 class="border-bottom border-gray pb-2 mb-0">Events</h6>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        /**
         * @var $events \App\Src\Event\EventModelCollection
         * @var $this  \App\Engine\View
         */
        $events = $this->get('events');;
        foreach ($events as $key => $event) :
            ?>
            <tr>
                <th scope="row"><?= $key ?></th>
                <td><?= $event->getName() ?></td>
                <td>
                    <a href="<?= $this->generateUrl('eventDetails', ['id' => $event->getId()]) ?>" class="btn btn-primary">Details</a>
                </td>
                <td>
                    <a href="<?= $this->generateUrl('eventsDelete', ['id' => $event->getId()]) ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
        </tbody>
    </table>
</div>