<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="<?= $this->generateUrl('pagesDetails', ['id' => $this->page->getId()]) ?>"
                   class="btn btn-sm btn-outline-secondary">Back</a>
            </div>

        </div>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    /**
     * @var $page  \App\Src\Page\PageModel
     * @var $event \App\Src\Event\EventModel
     * @var $user  \App\Src\User\UserModel
     */
    $page = $this->get('page');
    $event = $this->get('event');
    $user = $this->get('user');
    ?>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Details</h6>
        <table class="table table-striped">
            <tbody>
            <tr>
                <th>Page name:</th>
                <td><?= $page->getName(); ?></td>
            </tr>
            <tr>
                <th>Event name:</th>
                <td><?= $event->getName(); ?></td>
            </tr>
            <tr>
                <th>Created at:</th>
                <td><?= $event->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
            </tr>
            <tr>
                <th>Deleted at:</th>
                <td><?= $event->getDeletedAt() ? $event->getDeletedAt()->format('Y-m-d H:i:s') : '' ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Code</h6>
        <samp>
            &lt;<code>img</code> src="<?= $this->generateUrl('pixel', ['hash' => $event->getHash()]) ?>"&gt;
        </samp>
    </div>
    <?php if ($this->get('file')) : ?>
        <div class="my-3 p-3 box rounded box-shadow">
            <h6 class="border-bottom border-gray pb-2 mb-0">File</h6>
            <img src="<?= $this->get('file'); ?>" class="img-thumbnail" alt="Event image">
        </div>
    <?php endif; ?>
    <div class="my-3 p-3 box rounded box-shadow" id="event-calls">
        <h6 class="border-bottom border-gray pb-2 mb-0">Event calls</h6>
        <img class="img-center" src="<?= $this->getAssetsUrl('img/ajax-loader.gif'); ?>" alt="Loading">
    </div>

</main>