<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    ?>
    <form action="<?= $this->generateUrl('pagesAddNew') ?>" method="post">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input
                        type="text"
                        class="form-control"
                        name="pageName"
                        placeholder="Name"
                    <?= $this->get('pageName') ? 'value="' . $this->get('pageName') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Address</label>
            <div class="col-sm-10">
                <input
                        type="url"
                        class="form-control"
                        name="pageUrl"
                        placeholder="Url"
                    <?= $this->get('pageUrl') ? 'value="' . $this->get('pageUrl') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </div>
    </form>
</main>