<div class="my-3 p-3 box rounded box-shadow">
    <h6 class="border-bottom border-gray pb-2 mb-0">My Pages</h6>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Url</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        /**
         * @var $pages \App\Src\Page\PageModelCollection
         * @var $this  \App\Engine\View
         */
        $pages = $this->get('pages');
        foreach ($pages as $key => $page) :
            ?>
            <tr>
                <th scope="row"><?= $key ?></th>
                <td><?= $page->getName() ?></td>
                <td><?= $page->getUrl() ?></td>
                <td>
                    <a href="<?= $this->generateUrl('pagesDetails', ['id' => $page->getId()]) ?>" class="btn btn-info">Details</a>
                </td>
                <td>
                    <a href="<?= $this->generateUrl('pagesDelete', ['id' => $page->getId()]) ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
        </tbody>
    </table>
</div>