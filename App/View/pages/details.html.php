<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    /**
     * @var $page \App\Src\Page\PageModel
     * @var $user \App\Src\User\UserModel
     */
    $page = $this->page;
    $user = $this->user;
    ?>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Details</h6>
        <table class="table table-striped">
            <tbody>
            <tr>
                <th>Name:</th>
                <td><?= $page->getName(); ?></td>
            </tr>
            <tr>
                <th>Url:</th>
                <td><?= $page->getUrl(); ?></td>
            </tr>
            <tr>
                <th>Owner:</th>
                <td><?= $user->getEmail(); ?></td>
            </tr>
            <tr>
                <th>Created at:</th>
                <td><?= $page->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
            </tr>
            <tr>
                <th>Deleted at:</th>
                <td><?= $page->getDeletedAt() ? $page->getDeletedAt()->format('Y-m-d H:i:s') : '' ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="my-3 p-3 box rounded box-shadow">
        <a href="<?= $this->generateUrl('eventsAddNewForPage', ['pageId' => $page->getId()]) ?>" class="btn btn-primary">Add new Event</a>
    </div>
    <?php
    $this->renderHTML('eventsTable', 'events/', false);
    ?>

</main>