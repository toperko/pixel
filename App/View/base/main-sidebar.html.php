<?php
/**
 * @var $this \App\Engine\View
 */
?>
<nav class="col-md-2 d-none d-md-block sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('homePage'); ?>">
                    <span data-feather="home"></span>
                    Dashboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('pagesIndex'); ?>">
                    <span data-feather="file-text"></span>
                    Pages
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('pagesAddNew'); ?>">
                    <span data-feather="file-plus"></span>
                    Add new page
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('eventsAddNew'); ?>">
                    <span data-feather="plus"></span>
                    Add new event
                </a>
            </li>
        </ul>
        <?php if ($this->get('user') && $this->get('user')->getLevel() >= \App\Src\User\UserConst::ADMIN) : ?>
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>Admin panel</span>
            </h6>
            <ul class="nav flex-column mb-2">
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->generateUrl('adminUsersList'); ?>">
                        <span data-feather="users"></span>
                        Users
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->generateUrl('adminUsersAdd'); ?>">
                        <span data-feather="user-plus"></span>
                        Add new users
                    </a>
                </li>
            </ul>
        <?php endif; ?>
    </div>
</nav>