<nav class="navbar navbar-dark fixed-top flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="<?= $this->generateUrl('homePage') ?>"><?= $this->settings->applicationName ?></a>
    <ul class="navbar-nav">

        <li class="nav-item">
            <a class="nav-link" href="<?= $this->generateUrl('changeTheme') ?>"><?= $this->t('changeTheme') ?></a>
        </li>
        <?php if (isset($this->user)): ?>

            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('logout') ?>"><?= $this->user->getEmail() ?></a>
            </li>

        <?php endif; ?>
        <?php foreach ($this->translation->getInstalledLang() as $lang): ?>

            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('changeLang', ['lang' => $lang]) ?>"><?= $lang ?></a>
            </li>

        <?php endforeach; ?>

        <li class="nav-item">
            <a class="nav-link" href="<?= $this->generateUrl('logout') ?>"><?= $this->t('signOut') ?></a>
        </li>
    </ul>
</nav>