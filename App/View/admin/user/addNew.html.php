<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    ?>
    <form action="<?= $this->generateUrl('adminUsersAdd') ?>" method="post">
        <div class="form-group row">
            <label for="inputNewEmail" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input
                        id="inputNewEmail"
                        type="email"
                        class="form-control"
                        name="newEmail"
                        placeholder="Email"
                    <?= $this->get('email') ? 'value="' . $this->get('email') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputNewPassword" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input
                        id="inputNewPassword"
                        type="password"
                        class="form-control"
                        name="newPassword"
                        placeholder="Password"
                    <?= $this->get('password') ? 'value="' . $this->get('password') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputUserType" class="col-sm-2 col-form-label">User type</label>
            <div class="col-sm-10">
                <select id="inputUserType" class="form-control" name="userType">
                    <?php
                    foreach (\App\Src\User\UserConst::USER_NAMES as $key => $name) {
                        if ($key == 0) {
                            continue;
                        }
                        echo '<option 
                                value="' . $key . '" 
                                ' . (($this->get('userType') == $key) ? 'selected' : '') . '
                               >' . $name . '</option>';
                    }
                    ?>

                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </div>
    </form>
</main>