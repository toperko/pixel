<?php
namespace App;

use function strlen;
use function rand;

/**
 * Class Helper
 *
 * @package App
 */
class Helper
{
    /**
     * @param int $length
     *
     * @return string
     */
    public static function randomHash(int $length = 20) : string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
