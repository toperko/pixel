<?php
$container = $app->getContainer();
$container['db'] = function($container) {
    $settings = $container->get('settings');
    $db = $settings['db'];
    if (!$db) {
        throw new Exception('Brak klucza db w settings.php');
    }
    $pdo = new \PDO(
        $db['driver'] . ':host=' . $db['host'] . ';dbname=' . $db['dbname'] . ';charset=' . $db['charset'] . ';port=' . $db['port'],
        $db['user'],
        $db['pass']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);

    return $pdo;
};
$container['router'] = function(\Psr\Container\ContainerInterface $container) {
    $url = HTTP_SERVER . $_SERVER['REQUEST_URI'];

    return new App\Engine\Router\Router(
        $url,
        \App\Engine\Router\RouteCollection::getInstance(),
        $container->get('userService'),
        $container->get('session')
    );
};
$container['session'] = function() {
    return new App\Engine\Session\Session();
};
$container['cookie'] = function() {
    return new App\Engine\Cookie();
};
$container['translation'] = function(\Psr\Container\ContainerInterface $container) {
    return new App\Engine\Translation($container->get('cookie'));
};
$container['view'] = function(\Psr\Container\ContainerInterface $container) {
    return new App\Engine\View(
        $container->get('translation'),
        $container->get('session'),
        $container->get('cookie'),
        $container->get('router'),
        $container->get('settings')->all()
    );
};
$container['cache'] = function() {
    return new \Symfony\Component\Cache\Simple\FilesystemCache();
};
/**
 * Users
 */
$container['userSessionService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserSessionService($container->get('session'));
};
$container['userCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserCommandRepository($container->get('db'));
};
$container['userQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserQueryRepository($container->get('db'));
};
$container['userService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserService(
        $container->get('userSessionService'),
        $container->get('userCommandRepository'),
        $container->get('userQueryRepository')
    );
};
/**
 * Pages
 */
$container['pageCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Page\PageCommandRepository($container->get('db'));
};
$container['pageQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Page\PageQueryRepository($container->get('db'));
};
$container['pageService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Page\PageService(
        $container->get('pageCommandRepository'),
        $container->get('pageQueryRepository')
    );
};
/**
 * Events
 */
$container['eventCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Event\EventCommandRepository($container->get('db'));
};
$container['eventQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Event\EventQueryRepository($container->get('db'));
};
$container['eventService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Event\EventService(
        $container->get('eventCommandRepository'),
        $container->get('eventQueryRepository')
    );
};
/**
 * Event Call
 */
$container['eventCallCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\EventCalls\EventCallCommandRepository($container->get('db'));
};
$container['eventCallQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\EventCalls\EventCallQueryRepository($container->get('db'));
};
$container['eventCallService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\EventCalls\EventCallService(
        $container->get('eventCallCommandRepository'),
        $container->get('eventCallQueryRepository')
    );
};
/**
 * Device-detector
 */
$container['deviceDetector'] = function(\Psr\Container\ContainerInterface $container) {
    $deviceDetector = new \DeviceDetector\DeviceDetector($_SERVER['HTTP_USER_AGENT']);
    $deviceDetector->setCache(
        new \DeviceDetector\Cache\PSR16Bridge($container->get('cache'))
    );

    return $deviceDetector;
};
