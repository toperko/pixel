<?php
return [
    'settings' => [
        'applicationHost'        => 'localhost',
        'applicationPort'        => 8000,
        'applicationName'        => 'Pixel',
        'displayErrorDetails'    => true,
        'addContentLengthHeader' => false,
        'db'                     => [
            'driver'  => "mysql",
            'host'    => "mysql",
            'port'    => 3306,
            'user'    => "root",
            'pass'    => "root",
            'dbname'  => "pixel",
            'charset' => 'utf8',
        ],
        // Monolog settings
        'logger'                 => [
            'name'  => 'slim-app',
            'path'  => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../Logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'view'                   => [
            'dir' => __DIR__ . '/../View',
        ],
    ],
];
