<?php
use App\Engine\Router\RouteCollection;
use App\Engine\Router\Route;

$collection = RouteCollection::getInstance();
$collection->add('homePage', new Route(
    HTTP_SERVER. '/',
    [
        'class'     => '\Index',
        'method'    => 'index',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('404', new Route(
    HTTP_SERVER . '/404',
    [
        'class'  => '\Error',
        'method' => 'error404',
    ]
));
$collection->add('login', new Route(
    HTTP_SERVER . '/login',
    [
        'class'  => '\User',
        'method' => 'loginAction',
    ]
));
$collection->add('logout', new Route(
    HTTP_SERVER . '/logout',
    [
        'class'  => '\User',
        'method' => 'logOutAction',
    ]
));
$collection->add('changeLang', new Route(
    HTTP_SERVER . '/language/<lang>',
    [
        'class'  => '\Language',
        'method' => 'change',
    ],
    [
        'lang' => '(\w{2,2})$',
    ]
));
$collection->add('changeTheme', new Route(
    HTTP_SERVER . '/theme',
    [
        'class'  => '\Template',
        'method' => 'switchTheme',
    ]
));
$collection->add('pagesIndex', new Route(
    HTTP_SERVER . '/pages',
    [
        'class'     => '\Pages',
        'method'    => 'viewPages',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('pagesAddNew', new Route(
    HTTP_SERVER . '/pages/add',
    [
        'class'     => '\Pages',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('pagesDelete', new Route(
    HTTP_SERVER . '/page/delete/<id>',
    [
        'class'     => '\Pages',
        'method'    => 'deletePage',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'id' => '([1-9][0-9]*)',
    ]
));
$collection->add('pagesDetails', new Route(
    HTTP_SERVER . '/page/<id>',
    [
        'class'     => '\Pages',
        'method'    => 'showDetails',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'id' => '([1-9][0-9]*)',
    ]
));
$collection->add('eventsAddNew', new Route(
    HTTP_SERVER . '/events/add',
    [
        'class'     => '\Events',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('eventsAddNewForPage', new Route(
    HTTP_SERVER . '/page/<pageId>/event/add',
    [
        'class'     => '\Events',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'pageId' => '([1-9][0-9]*)',
    ]
));
$collection->add('eventsDelete', new Route(
    HTTP_SERVER . '/event/delete/<id>',
    [
        'class'     => '\Events',
        'method'    => 'deleteEvent',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'id' => '([1-9][0-9]*)',
    ]
));
$collection->add('pixel', new Route(
    HTTP_SERVER . '/pixel/<hash>.png',
    [
        'class'  => '\Pixel',
        'method' => 'pixel',
    ],
    [
        'hash' => '(\w+)',
    ]
));
$collection->add('eventDetails', new Route(
    HTTP_SERVER . '/event/<id>',
    [
        'class'     => '\Events',
        'method'    => 'details',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'id' => '([1-9][0-9]*)',
    ]
));
$collection->add('getEventCalls', new Route(
    HTTP_SERVER . '/event/<id>/calls',
    [
        'class'     => '\EventCalls',
        'method'    => 'getEventCalls',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'eventId' => '([1-9][0-9]*)',
    ],
    [
        'page' => '1',
    ]
));
$collection->add('getEventCallsPage', new Route(
    HTTP_SERVER . '/event/<id>/calls/<page>',
    [
        'class'     => '\EventCalls',
        'method'    => 'getEventCalls',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'eventId' => '([1-9][0-9]*)',
        'page' => '([1-9][0-9]*)',
    ]
));
$collection->add('adminUsersList', new Route(
    HTTP_SERVER . '/admin/users',
    [
        'class'     => '\Admin\User',
        'method'    => 'viewUsers',
        'authLevel' => \App\Src\User\UserConst::ADMIN,
    ]
));
$collection->add('adminUsersAdd', new Route(
    HTTP_SERVER . '/admin/user/add',
    [
        'class'     => '\Admin\User',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::ADMIN,
    ]
));
