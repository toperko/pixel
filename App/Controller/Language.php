<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\Controller;
use function in_array;

/**
 * Class Language
 *
 * @package App\Controller
 */
class Language extends Controller
{
    /**
     * @throws \Exception
     */
    public function change()
    {
        $lang = $this->get('lang', FILTER_SANITIZE_STRING);
        if (isset($lang) && in_array($lang, $this->translation->getInstalledLang())) {
            $this->translation->setLang($lang);
        }
        $this->app->redirect($this->generateUrl('homePage'));
    }
}
