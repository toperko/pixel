<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\App;
use App\Engine\Controller;
use App\Engine\Session\FlashMessage;
use App\Src\Event\EventCreateCommand;
use App\Src\Event\EventModel;
use App\Src\Event\EventService;
use App\Src\EventCalls\EventCallService;
use App\Src\Page\PageService;
use Exception;
use Throwable;
use function pathinfo;
use function move_uploaded_file;
use function file_exists;

/**
 * Class Events
 *
 * @package App\Controller
 */
class Events extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var PageService
     */
    private $pageService;

    /**
     * @var EventCallService
     */
    private $eventCallService;

    /**
     * @const int
     */
    private const FILE_MAX_SIZE = 2000000;

    /**
     * @const array
     */
    private const FILE_ACCEPT = ['png'];

    /**
     * Events constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->pageService = $app->getContainer()->get('pageService');
        $this->eventService = $app->getContainer()->get('eventService');
        $this->eventCallService = $app->getContainer()->get('eventCallService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws Throwable
     */
    public function add()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Add new event');
        $view->set(
            'pages',
            $this->pageService->getPagesByUserId(
                $this->userService->getUserLogin()->getId()
            )
        );
        if ($this->get('pageId')) {
            $view->set('pageId', $this->get('pageId', FILTER_VALIDATE_INT));
        }

        if ($this->isGet()) {
            $view->renderHTML('add', 'events/');

            return;
        }
        try {
            $this->beginDbTransaction();
            $view->set('pageId', $this->post('pageId', FILTER_VALIDATE_INT));
            $view->set('eventName', $this->post('eventName', FILTER_SANITIZE_STRING));
            $eventCreateCommand = (new EventCreateCommand())
                ->setPageId($this->post('pageId', FILTER_VALIDATE_INT))
                ->setName($this->post('eventName', FILTER_SANITIZE_STRING));
            if (!$eventCreateCommand->valid()) {
                foreach ($eventCreateCommand->getErrors() as $error) {
                    $this->app->getSession()->addFlash(
                        (new FlashMessage())
                            ->setType(FlashMessage::DANGERS_TYPE)
                            ->setMessage($error)
                    );
                }
                $view->renderHTML('add', 'events/');

                return;
            }
            $createdId = $this->eventService->create($eventCreateCommand);
            if ($file = $this->file('img')) {
                $this->uploadFile($file, $this->eventService->getById($createdId));
            }
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Event added')
            );
            $this->commitDbTransaction();
            $this->app->redirect($this->generateUrl('pagesDetails', ['id' => $eventCreateCommand->getPageId()]));
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        }
        $view->renderHTML('add', 'events/');
    }

    /**
     * @param array      $file
     * @param EventModel $eventModel
     *
     * @return bool
     * @throws \Exception
     */
    private function uploadFile(array $file, EventModel $eventModel) : bool
    {
        $fileName = $file['name'];
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $size = $file['size'];
        $tmpName = $file['tmp_name'];
        $uploadName = DIR_UPLOAD . '/' . $eventModel->getHash() . '.' . $extension;
        if ($size > self::FILE_MAX_SIZE) {
            throw new Exception('File is too big');
        }
        if (!in_array($extension, self::FILE_ACCEPT)) {
            throw new Exception('Accept only png files');
        }
        if (move_uploaded_file($tmpName, $uploadName)) {
            return true;
        }
        throw new Exception('Cant upload file');
    }

    /**
     * @throws \ReflectionException
     */
    public function deleteEvent()
    {
        try {
            $this->beginDbTransaction();
            $user = $this->userService->getUserLogin();
            $eventId = $this->get('id', FILTER_VALIDATE_INT);
            $event = $this->eventService->getById($eventId);
            if (!$event) {
                throw new Exception('Cant find this event');
            }
            $page = $this->pageService->getById($event->getPageId());
            if ($page->getOwnerId() !== $user->getId()) {
                throw new Exception('Only owner can delete event');
            }
            $eventCallsDeleted = $this->eventCallService->deleteByEventId($event->getId());
            $this->eventService->delete($event->getId());
            $this->commitDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage(sprintf('Deleted %d Event calls', $eventCallsDeleted))
            );
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Event deleted')
            );
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        } finally {
            if (isset($event)) {
                $this->app->redirect($this->generateUrl('pagesDetails', ['id' => $event->getPageId()]));
            }
            $this->app->redirect($this->generateUrl('pagesIndex'));
        }
    }

    /**
     * @throws \ReflectionException
     */
    public function details()
    {
        try {
            $eventId = $this->get('id', FILTER_VALIDATE_INT);
            $event = $this->eventService->getById($eventId);
            if (!$event) {
                throw new Exception('Cant find this event');
            }
            $page = $this->pageService->getById($event->getPageId());
            if ($page->getOwnerId() !== $this->userService->getUserLogin()->getId()) {
                throw new Exception('Only owner can see event');
            }
            $view = $this->getView();
            $view->addJs('eventDetailsScript', 'eventDetailsScript.js');
            $view->set('pageTitle', 'Event: ' . $event->getName());
            $view->set('page', $page);
            $view->set('event', $event);
            $view->set('file', $this->getFileForEvent($event->getHash()));
            $view->renderHTML('eventDetails', 'events/');
        } catch (\Throwable $exception) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
            if (isset($page)) {
                $this->app->redirect($this->generateUrl('pagesDetails', ['id' => $page->getId()]));
            }
            $this->app->redirect($this->generateUrl('pagesIndex'));
        }
    }

    /**
     * @param string $hash
     *
     * @return null|string
     */
    public function getFileForEvent(string $hash) : ?string
    {
        if (file_exists(DIR_UPLOAD . '/' . $hash . '.png')) {
            return $this->generateUrl('upload/' . $hash . '.png');
        }

        return null;
    }
}
