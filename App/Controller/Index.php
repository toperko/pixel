<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\App;
use App\Engine\Controller;
use App\Src\Event\EventService;
use App\Src\EventCalls\EventCallService;
use App\Src\Page\PageService;

/**
 * Class Index
 *
 * @package App\Controller
 */
class Index extends Controller
{
    /**
     * @var PageService
     */
    private $pageService;

    /**
     * @var EventCallService
     */
    private $eventCallService;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * Index constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->pageService = $app->getContainer()->get('pageService');
        $this->eventCallService = $app->getContainer()->get('eventCallService');
        $this->eventService = $app->getContainer()->get('eventService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function index()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Dashboard');
        $view->set('pages', $this->pageService->getPagesByUserId(
            $this->userService->getUserLogin()->getId()
        ));
        $view->set('lastEventCall', $this->eventCallService->getLastByUserId(
            $this->userService->getUserLogin()->getId()
        ));
        if ($view->get('lastEventCall')) {
            $view->set('lastEvent', $this->eventService->getById(
                $view->get('lastEventCall')->getEventId()
            ));
            $view->set('lastPage', $this->pageService->getById(
                $view->get('lastEvent')->getPageId()
            ));
        }
        $view->renderHTML('index', 'index/');
    }
}
