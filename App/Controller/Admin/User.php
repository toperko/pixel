<?php
declare(strict_types=1);
namespace App\Controller\Admin;

use App\Engine\Controller;
use App\Engine\Session\FlashMessage;
use App\Src\User\UserCreateCommand;
use Throwable;
use Exception;

/**
 * Class User
 *
 * @package App\Controller\Admin
 */
class User extends Controller
{
    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function viewUsers() : void
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Users');
        $view->set('users', $this->userService->getAll());
        $view->renderHTML('userList', 'admin/user/');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function add() : void
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Add new user');
        $view->set('email', $this->post('newEmail', FILTER_SANITIZE_EMAIL));
        $view->set('password', $this->post('newPassword'));
        $view->set('userType', $this->post('userType', FILTER_SANITIZE_NUMBER_INT));
        if ($this->isGet()) {
            $view->renderHTML('addNew', 'admin/user/');

            return;
        }
        try {
            $this->beginDbTransaction();
            $userCreateCommand = (new UserCreateCommand())
                ->setEmail($this->post('newEmail', FILTER_SANITIZE_EMAIL))
                ->setPassword($this->post('newPassword'))
                ->setLevel($this->post('userType', FILTER_VALIDATE_INT))
                ->setCreatedBy($this->userService->getUserLogin()->getId());
            if (!$userCreateCommand->valid()) {
                foreach ($userCreateCommand->getErrors() as $error) {
                    $this->app->getSession()->addFlash(
                        (new FlashMessage())
                            ->setType(FlashMessage::DANGERS_TYPE)
                            ->setMessage($error)
                    );
                }
                $view->renderHTML('addNew', 'admin/user/');

                return;
            }
            if ($userCreateCommand->getLevel() > $this->userService->getUserLogin()->getLevel()) {
                throw new Exception('You cant create this user type.');
            }
            $this->userService->create($userCreateCommand);
            $this->commitDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Page added')
            );
            $this->app->redirect($this->generateUrl('adminUsersList'));
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );

        }
        $view->renderHTML('addNew', 'admin/user/');
    }
}
