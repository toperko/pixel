<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\Controller;
use App\Engine\Session\FlashMessage;

/**
 * Class User
 *
 * @package App\Controller
 */
class User extends Controller
{
    /**
     *  Akcja do linku
     */
    public function logOutAction()
    {
        $this->userService->logOut();
        $this->app->redirect($this->generateUrl('login'));
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function loginAction()
    {
        if ($this->userService->checkUserIsLogin()) {
            $this->app->redirect($this->generateUrl('homePage'));
        }
        $view = $this->getView();
        $view->addCss('loginCss', 'pages/loginPage.css');
        if ($this->isPost()) {
            $email = $this->post('email', FILTER_SANITIZE_EMAIL);
            $password = $this->post('password');
            $rememberMe = $this->post('rememberMe') !== null;
            if ($user = $this->userService->getUserByEmailAndPassword($email, $password)) {
                $this->userService->userLogin($user, $rememberMe);
                $this->userService->updateLastLogin($user->getId());
                if ($this->app->getSession()->has('redirect')) {
                    $url = $this->app->getSession()->get('redirect');
                    $this->app->getSession()->remove('redirect');
                    $this->app->redirect($url);

                    return;
                }
                $this->app->redirect($this::generateUrl('homePage'));

                return;
            }
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setName('wrongLoginData')
                    ->setTitle('Wrong login data.')
                    ->setMessage('Incorrect email address or password.')
            );
            $view->renderHTML('login', 'login/', false);

            return;
        }
        $view->renderHTML('login', 'login/', false);

        return;
    }
}
