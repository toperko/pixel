<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\App;
use App\Engine\Controller;
use App\Src\Event\EventService;
use App\Src\EventCalls\EventCallService;
use App\Src\Page\PageService;
use function http_response_code;
use function ceil;

/**
 * Class EventCalls
 *
 * @package App\Controller
 */
class EventCalls extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var PageService
     */
    private $pageService;

    /**
     * @var EventCallService
     */
    private $eventCallService;

    /**
     * @const int
     */
    public const EVENT_PER_PAGE = 10;

    /**
     * EventCalls constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->pageService = $app->getContainer()->get('pageService');
        $this->eventService = $app->getContainer()->get('eventService');
        $this->eventCallService = $app->getContainer()->get('eventCallService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function getEventCalls()
    {
        $eventId = $this->get('eventId', FILTER_VALIDATE_INT);
        $page = $this->get('page', FILTER_VALIDATE_INT);
        $event = $this->eventService->getById($eventId);
        if (!$event) {
            http_response_code(404);
            $this->getView()->renderJSON(['error' => 'Cant find this event']);
        }
        $pageModel = $this->pageService->getById($event->getPageId());
        if ($pageModel->getOwnerId() !== $this->userService->getUserLogin()->getId()) {
            http_response_code(400);
            $this->getView()->renderJSON(['error' => 'Only owner can see event']);
        }
        $offset = ($page - 1) * self::EVENT_PER_PAGE;
        $eventCalls = $this->eventCallService->getByEventId($event->getId(), self::EVENT_PER_PAGE, $offset);
        $eventCallsCount = $this->eventCallService->countCallsByEventId($event->getId());
        $pageMax = ceil($eventCallsCount / self::EVENT_PER_PAGE);
        $this->getView()->renderJSON([
            'count'       => $eventCallsCount,
            'pageCurrent' => $page,
            'pageMax'     => $pageMax,
            'data'        => $eventCalls->toArray(),
        ]);
    }
}
