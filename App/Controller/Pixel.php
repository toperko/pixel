<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\App;
use App\Engine\Controller;
use App\Src\Event\EventService;
use App\Src\EventCalls\EventCallCreateCommand;
use App\Src\EventCalls\EventCallService;
use DeviceDetector\DeviceDetector;
use function http_response_code;
use function file_exists;
use function ob_clean;
use function header;
use function filesize;
use function fpassthru;
use function fopen;
use function imagecreate;
use function imagecolorallocate;
use function imagepng;
use function imagedestroy;
use function substr;

/**
 * Class Pixel
 *
 * @package App\Controller
 */
class Pixel extends Controller
{
    /**
     * @var EventCallService
     */
    private $eventCallService;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var DeviceDetector
     */
    private $deviceDetector;

    /**
     * Pixel constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->eventService = $app->getContainer()->get('eventService');
        $this->eventCallService = $app->getContainer()->get('eventCallService');
        $this->deviceDetector = $app->getContainer()->get('deviceDetector');
        $this->deviceDetector->parse();
    }

    /**
     * @throws \App\Engine\Exception\ValidationException
     * @throws \Exception
     */
    public function pixel()
    {
        $hash = $this->get('hash', FILTER_SANITIZE_STRING);
        $event = $this->eventService->getByHash($hash);
        if (!$event || $event->getDeletedAt()) {
            http_response_code(404);
            exit;
        }
        $clientInfo = $this->getClientInfo();
        $eventCallCreateCommand = (new EventCallCreateCommand())
            ->setEventId($event->getId())
            ->setIp($clientInfo['ip'])
            ->setLang($clientInfo['lang'])
            ->setDeviceBrand($clientInfo['deviceBrand'])
            ->setDeviceModel($clientInfo['deviceModel'])
            ->setDeviceOs($clientInfo['deviceOs'])
            ->setClientName($clientInfo['clientName'])
            ->setClientVersion($clientInfo['clientVersion']);
        $this->eventCallService->create($eventCallCreateCommand);
        if (!file_exists(DIR_UPLOAD . '/' . $hash . '.png')) {
            $this->generatePngPixel();
        }
        $this->showUploadedFile(DIR_UPLOAD . '/' . $hash . '.png');

    }

    /**
     * @param $file
     */
    private function showUploadedFile($file) : void
    {
        ob_clean();
        header("Content-Type: image/png");
        header("Content-Length: " . filesize($file));
        fpassthru(fopen($file, 'rb'));
        exit;
    }

    /**
     * Generate 1x1 png image
     */
    private function generatePngPixel() : void
    {
        ob_clean();
        header("Content-Type: image/png");
        $im = imagecreate(1, 1);
        imagecolorallocate($im, 0, 0, 0);
        imagepng($im);
        imagedestroy($im);
        exit;
    }

    /**
     * @return array
     */
    private function getClientInfo() : array
    {
        return [
            'ip'            => $_SERVER['REMOTE_ADDR'],
            'lang'          => substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 5),
            'deviceBrand'   => $this->deviceDetector->getBrandName(),
            'deviceModel'   => $this->deviceDetector->getDeviceName(),
            'deviceOs'      => $this->deviceDetector->getOs('name') . ' ' . $this->deviceDetector->getOs('platform'),
            'clientName'    => $this->deviceDetector->getClient('name'),
            'clientVersion' => $this->deviceDetector->getClient('version'),
        ];
    }
}
