<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\App;
use App\Engine\Controller;
use App\Engine\Session\FlashMessage;
use App\Src\Event\EventService;
use App\Src\Page\PageCreateCommand;
use App\Src\Page\PageService;
use Exception;
use Throwable;

/**
 * Class Pages
 *
 * @package App\Controller
 */
class Pages extends Controller
{
    /**
     * @var PageService
     */
    private $pageService;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * Pages constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->pageService = $app->getContainer()->get('pageService');
        $this->eventService = $app->getContainer()->get('eventService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws Throwable
     */
    public function add()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Add new page');
        if ($this->isGet()) {
            $view->renderHTML('add', 'pages/');

            return;
        }
        try {
            $this->beginDbTransaction();
            $view->set('pageName', $this->post('pageName', FILTER_SANITIZE_STRING));
            $view->set('pageUrl', $this->post('pageUrl', FILTER_SANITIZE_URL));
            $pageCreateCommand = (new PageCreateCommand())
                ->setName($this->post('pageName', FILTER_SANITIZE_STRING))
                ->setUrl($this->post('pageUrl', FILTER_SANITIZE_URL))
                ->setOwnerId($this->userService->getUserLogin()->getId());
            if (!$pageCreateCommand->valid()) {
                foreach ($pageCreateCommand->getErrors() as $error) {
                    $this->app->getSession()->addFlash(
                        (new FlashMessage())
                            ->setType(FlashMessage::DANGERS_TYPE)
                            ->setMessage($error)
                    );
                }
                $view->renderHTML('add', 'pages/');

                return;
            }
            $this->pageService->create($pageCreateCommand);
            $this->commitDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Page added')
            );
            $this->app->redirect($this->generateUrl('pagesIndex'));
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        }
        $view->renderHTML('add', 'pages/');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function viewPages()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Pages');
        $user = $this->userService->getUserLogin();
        $pages = $this->pageService->getPagesByUserId($user->getId());
        $view->set('pages', $pages);
        $view->renderHTML('index', 'pages/');
    }

    /**
     * @throws \ReflectionException
     */
    public function deletePage()
    {
        try {
            $this->beginDbTransaction();
            $user = $this->userService->getUserLogin();
            $pageId = $this->get('id', FILTER_VALIDATE_INT);
            $page = $this->pageService->getById($pageId);
            if (!$page) {
                throw new Exception('Cant find this page');
            }
            if ($page->getOwnerId() !== $user->getId()) {
                throw new Exception('Only owner can delete page');
            }
            if (!$this->eventService->getByPageId($page->getId())->isEmpty()) {
                throw new Exception('This page have active events');
            }
            $this->pageService->delete($pageId);
            $this->commitDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Page deleted')
            );
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        } finally {
            $this->app->redirect($this->generateUrl('pagesIndex'));
        }
    }

    /**
     * @throws \ReflectionException
     */
    public function showDetails()
    {
        try {
            $user = $this->userService->getUserLogin();
            $pageId = $this->get('id', FILTER_VALIDATE_INT);
            $page = $this->pageService->getById($pageId);
            if (!$page) {
                throw new Exception('Cant find this page');
            }
            if ($page->getOwnerId() !== $user->getId()) {
                throw new Exception('Only owner see page details');
            }
            $view = $this->getView();
            $view->set('page', $page);
            $view->set(
                'events',
                $this->eventService->getByPageId($page->getId())
            );
            $view->set('pageTitle', 'Page: ' . $page->getName());
            $view->renderHTML('details', 'pages/');
        } catch (Throwable $exception) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
            $this->app->redirect($this->generateUrl('pagesIndex'));
        }
    }
}
