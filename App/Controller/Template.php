<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\App;
use App\Engine\Controller;
use App\Engine\Cookie;
use App\Engine\View;

/**
 * Class Template
 *
 * @package App\Controller
 */
class Template extends Controller
{
    /**
     * @var Cookie
     */
    private $cookie;

    /**
     * Template constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->cookie = $app->getContainer()->get('cookie');
    }

    /**
     * Switch theme endpoint
     */
    public function switchTheme()
    {
        if (
            !$this->cookie->has(View::THEME_COOKIE_KEY) ||
            $this->cookie->get(View::THEME_COOKIE_KEY, FILTER_SANITIZE_STRING) === View::THEME_LIGHT
        ) {
            $this->cookie->remove(View::THEME_COOKIE_KEY);
            $this->cookie->set(View::THEME_COOKIE_KEY, View::THEME_DARK);
        } else {
            $this->cookie->remove(View::THEME_COOKIE_KEY);
            $this->cookie->set(View::THEME_COOKIE_KEY, View::THEME_LIGHT);
        }
        $this->app->redirect($this->generateUrl('homePage'));
    }
}
