<?php
declare(strict_types=1);
namespace App\Controller;

use App\Engine\Controller;
use function http_response_code;

/**
 * Class Error
 *
 * @package App\Controller
 */
Class Error extends Controller
{
    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function error404()
    {
        http_response_code(404);
        $view = $this->getView();
        $view->setSetting([
            'headerBar' => false,
            'sideMenu'  => false,
            'footer'    => false,
        ]);
        $view->renderHTML('404', 'error/', true);
    }
}
