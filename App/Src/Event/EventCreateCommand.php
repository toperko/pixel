<?php
declare(strict_types=1);
namespace App\Src\Event;

use App\Engine\Command;

/**
 * Class EventCreateCommand
 *
 * @package App\Src\Event
 */
class EventCreateCommand extends Command
{
    /**
     * @var int|null
     */
    private $pageId;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string
     */
    private $hash;

    /**
     * @return bool
     */
    public function valid() : bool
    {
        if (empty($this->pageId) || $this->pageId === 0) {
            $this->errors['page'] = 'Page cant be empty';
        }
        if (empty($this->name)) {
            $this->errors['name'] = 'Name cant be empty';
        }
        if (!empty($this->name) && strlen($this->name) < 3 || strlen($this->name) > 200) {
            $this->errors['nameLength'] = 'Name must contain from 3 to 200 characters';
        }

        return count($this->errors) === 0;
    }

    /**
     * @return int|null
     */
    public function getPageId() : ?int
    {
        return $this->pageId;
    }

    /**
     * @param int|null $pageId
     *
     * @return EventCreateCommand
     */
    public function setPageId(?int $pageId) : self
    {
        $this->pageId = $pageId;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     *
     * @return EventCreateCommand
     */
    public function setName(?string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash() : string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     *
     * @return EventCreateCommand
     */
    public function setHash(string $hash) : self
    {
        $this->hash = $hash;

        return $this;
    }
}
