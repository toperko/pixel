<?php
declare(strict_types=1);
namespace App\Src\Event;

use App\Engine\Common\Collection;

/**
 * Class EventModelCollection
 *
 * @package App\Src\Event
 *
 * @method EventModel current()
 */
class EventModelCollection extends Collection
{
    /**
     * EventModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(EventModel::class, null, 'id');
    }
}
