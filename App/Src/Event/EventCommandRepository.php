<?php
declare(strict_types=1);
namespace App\Src\Event;

/**
 * Class EventCommandRepository
 *
 * @package App\Src\Event
 */
class EventCommandRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * EventCommandRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param EventCreateCommand $eventCreateCommand
     *
     * @return int
     * @throws \Exception
     */
    public function create(EventCreateCommand $eventCreateCommand) : int
    {
        $statement = $this->db->prepare('INSERT INTO events (
            page_id,
            name,
            hash
        ) VALUES (
            :pageId,
            :name,
            :hash
        )');
        $result = $statement->execute([
            'pageId' => $eventCreateCommand->getPageId(),
            'name'   => $eventCreateCommand->getName(),
            'hash'   => $eventCreateCommand->getHash(),
        ]);
        if (!$result) {
            throw new \Exception('Cant create new event');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $statement = $this->db->prepare('UPDATE events SET deleted_at = NOW() WHERE id = :id');
        $result = $statement->execute([
            'id'   => $id,
        ]);
        if (!$result) {
            throw new \Exception('Cant delete event');
        }

        return (int) $this->db->lastInsertId();
    }
}
