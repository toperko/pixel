<?php
declare(strict_types=1);
namespace App\Src\Event;

use App\Engine\Exception\ValidationException;
use App\Helper;

/**
 * Class EventService
 *
 * @package App\Src\Event
 */
class EventService
{
    /**
     * @var EventCommandRepository
     */
    private $eventCommandRepository;

    /**
     * @var EventQueryRepository
     */
    private $eventQueryRepository;

    /**
     * EventService constructor.
     *
     * @param EventCommandRepository $eventCommandRepository
     * @param EventQueryRepository   $eventQueryRepository
     */
    public function __construct(EventCommandRepository $eventCommandRepository, EventQueryRepository $eventQueryRepository)
    {
        $this->eventCommandRepository = $eventCommandRepository;
        $this->eventQueryRepository = $eventQueryRepository;
    }

    /**
     * @param EventCreateCommand $eventCreateCommand
     *
     * @return int
     * @throws ValidationException
     * @throws \Exception
     */
    public function create(EventCreateCommand $eventCreateCommand) : int
    {
        if (!$eventCreateCommand->valid()) {
            throw new ValidationException('Cant create page', implode(', ', $eventCreateCommand->getErrors()));
        }
        do {
            $hash = Helper::randomHash(20);
        } while ($this->getByHash($hash));
        $eventCreateCommand->setHash($hash);

        return $this->eventCommandRepository->create($eventCreateCommand);
    }

    /**
     * @param string $eventHash
     *
     * @return EventModel|null
     * @throws \Exception
     */
    public function getByHash(string $eventHash) : ?EventModel
    {
        return $this->eventQueryRepository->getByHash($eventHash);
    }

    /**
     * @param int $eventId
     *
     * @return EventModel|null
     * @throws \Exception
     */
    public function getById(int $eventId) : ?EventModel
    {
        return $this->eventQueryRepository->getById($eventId);
    }

    /**
     * @param int $id
     *
     * @return EventModelCollection
     * @throws \ReflectionException
     */
    public function getByPageId(int $id) : EventModelCollection
    {
        return $this->eventQueryRepository->getByPageId($id);
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $event = $this->getById($id);
        if (!$event) {
            throw new \Exception('Cant find this event');
        }
        if ($event->getDeletedAt()) {
            throw new \Exception('Event is already deleted');
        }

        return $this->eventCommandRepository->delete($id);
    }
}
