<?php
declare(strict_types=1);
namespace App\Src\Event;

/**
 * Class EventQueryRepository
 *
 * @package App\Src\Event
 */
class EventQueryRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * EventQueryRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $eventId
     *
     * @return EventModel|null
     * @throws \Exception
     */
    public function getById(int $eventId) : ?EventModel
    {
        $statement = $this->db->prepare('SELECT
                 id,
                page_id,
                name,
                hash,
                created_at,
                deleted_at
            FROM events WHERE id = :eventId;
        ');
        $result = $statement->execute([
            'eventId' => $eventId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from events');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new EventModel($result);
    }

    /**
     * @param string $eventHash
     *
     * @return EventModel|null
     * @throws \Exception
     */
    public function getByHash(string $eventHash) : ?EventModel
    {
        $statement = $this->db->prepare('SELECT
                 id,
                page_id,
                name,
                hash,
                created_at,
                deleted_at
            FROM events WHERE hash = :eventHash;
        ');
        $result = $statement->execute([
            'eventHash' => $eventHash,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from events');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new EventModel($result);
    }

    /**
     * @param int $pageId
     *
     * @return EventModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getByPageId(int $pageId) : EventModelCollection
    {
        $eventModelCollection = new EventModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                page_id,
                name,
                hash,
                created_at,
                deleted_at
            FROM events 
            WHERE page_id = :pageId
            AND deleted_at IS NULL;
        ');
        $result = $statement->execute([
            'pageId' => $pageId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from events');
        }
        $result = $statement->fetchAll();

        return $eventModelCollection->buildFromArray($result);
    }

    /**
     * @param int $id
     *
     * @return EventModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getByOwnerId(int $id) : EventModelCollection
    {
        $eventModelCollection = new EventModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                page_id,
                name,
                hash,
                created_at,
                deleted_at
            FROM events 
            WHERE page_id = :pageId
            AND deleted_at IS NULL;
        ');
        $result = $statement->execute([
            'pageId' => $id,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from events');
        }
        $result = $statement->fetchAll();

        return $eventModelCollection->buildFromArray($result);
    }
}
