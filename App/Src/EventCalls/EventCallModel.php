<?php
declare(strict_types=1);
namespace App\Src\EventCalls;

use App\Engine\Model;

/**
 * Class eventCallModel
 *
 * @package App\Src\EventCalls
 */
class EventCallModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $eventId;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     */
    private $lang;

    /**
     * @var string
     */
    private $deviceBrand;

    /**
     * @var string
     */
    private $deviceModel;

    /**
     * @var string
     */
    private $deviceOs;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var string
     */
    private $clientVersion;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * eventCallModel constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setId($data['id']);
        $this->setEventId($data['event_id']);
        $this->setIp($data['ip']);
        $this->setLang($data['lang']);
        $this->setDeviceBrand($data['device_brand']);
        $this->setDeviceModel($data['device_model']);
        $this->setDeviceOs($data['device_os']);
        $this->setClientName($data['client_name']);
        $this->setClientVersion($data['client_version']);
        $this->setCreatedAt(new \DateTime($data['created_at']));
    }

    public function toArray() : array
    {
        return [
            'id' => $this->getId(),
            'eventId' => $this->getEventId(),
            'ip' => $this->getIp(),
            'lang' => $this->getLang(),
            'deviceBrand' => $this->getDeviceBrand(),
            'deviceModel' => $this->getDeviceModel(),
            'deviceOs' => $this->getDeviceOs(),
            'clientName' => $this->getClientName(),
            'clientVersion' => $this->getClientVersion(),
            'createdAt' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
        ];
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return eventCallModel
     */
    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getEventId() : int
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     *
     * @return eventCallModel
     */
    public function setEventId(int $eventId) : self
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * @return string
     */
    public function getIp() : string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return eventCallModel
     */
    public function setIp(string $ip) : self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return string
     */
    public function getLang() : string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     *
     * @return eventCallModel
     */
    public function setLang(string $lang) : self
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceBrand() : string
    {
        return $this->deviceBrand;
    }

    /**
     * @param string $deviceBrand
     *
     * @return eventCallModel
     */
    public function setDeviceBrand(string $deviceBrand) : self
    {
        $this->deviceBrand = $deviceBrand;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceModel() : string
    {
        return $this->deviceModel;
    }

    /**
     * @param string $deviceModel
     *
     * @return eventCallModel
     */
    public function setDeviceModel(string $deviceModel) : self
    {
        $this->deviceModel = $deviceModel;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceOs() : string
    {
        return $this->deviceOs;
    }

    /**
     * @param string $deviceOs
     *
     * @return eventCallModel
     */
    public function setDeviceOs(string $deviceOs) : self
    {
        $this->deviceOs = $deviceOs;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientName() : string
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName
     *
     * @return eventCallModel
     */
    public function setClientName(string $clientName) : self
    {
        $this->clientName = $clientName;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientVersion() : string
    {
        return $this->clientVersion;
    }

    /**
     * @param string $clientVersion
     *
     * @return eventCallModel
     */
    public function setClientVersion(string $clientVersion) : self
    {
        $this->clientVersion = $clientVersion;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return eventCallModel
     */
    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
