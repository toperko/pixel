<?php
declare(strict_types=1);
namespace App\Src\EventCalls;

/**
 * Class EventCallQueryRepository
 *
 * @package App\Src\EventCalls
 */
class EventCallQueryRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * EventCallQueryRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $userId
     *
     * @return EventCallModel|null
     * @throws \Exception
     */
    public function getLastByUserId(int $userId) : ?EventCallModel
    {
        $statement = $this->db->prepare('SELECT
                event_calls.id,
                event_calls.event_id,
                event_calls.ip,
                event_calls.lang,
                event_calls.device_brand,
                event_calls.device_model,
                event_calls.device_os,
                event_calls.client_name,
                event_calls.client_version,
                event_calls.created_at
            FROM pages 
            LEFT JOIN events on pages.id = events.page_id 
            JOIN event_calls on events.id = event_calls.event_id 
            where pages.owner_id = :userId 
            ORDER BY id DESC LIMIT 1;
        ');
        $result = $statement->execute([
            'userId' => $userId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from event calls');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new EventCallModel($result);
    }

    /**
     * @param int      $eventId
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return EventCallModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getByEventId(int $eventId, ?int $limit = null, ?int $offset = null) : EventCallModelCollection
    {
        $eventModelCollection = new EventCallModelCollection();
        $sql = 'SELECT
                id,
                event_id,
                ip,
                lang,
                device_brand,
                device_model,
                device_os,
                client_name,
                client_version,
                created_at
            FROM event_calls 
            WHERE event_id = :eventId
            ORDER BY id DESC
        ';
        if ($limit) {
            $sql .= ' LIMIT ' . $limit;
        }
        if ($offset) {
            $sql .= ' OFFSET ' . $offset;
        }
        $statement = $this->db->prepare($sql);
        $result = $statement->execute([
            'eventId' => $eventId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from event calls');
        }
        $result = $statement->fetchAll();

        return $eventModelCollection->buildFromArray($result);
    }

    /**
     * @param int $eventId
     *
     * @return int
     * @throws \Exception
     */
    public function countCallsByEventId(int $eventId) : int
    {
        $statement = $this->db->prepare('SELECT
                count(1)
            FROM event_calls
            where event_id = :eventId;
        ');
        $result = $statement->execute([
            'eventId' => $eventId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from event calls');
        }

        return $statement->fetch(\PDO::FETCH_COLUMN);
    }
}
