<?php
declare(strict_types=1);
namespace App\Src\EventCalls;

/**
 * Class EventCallCommandRepository
 *
 * @package App\Src\EventCalls
 */
class EventCallCommandRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * EventCallCommandRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param EventCallCreateCommand $eventCallCreateCommand
     *
     * @return int
     * @throws \Exception
     */
    public function create(EventCallCreateCommand $eventCallCreateCommand) : int
    {
        $statement = $this->db->prepare('INSERT INTO event_calls (
            event_id,
            ip,
            lang,
            device_brand,
            device_model,
            device_os,
            client_name,
            client_version
        ) VALUES (
            :eventId,
            :ip,
            :lang,
            :deviceBrand,
            :deviceModel,
            :deviceOs,
            :clientName,
            :clientVersion
        )');
        $result = $statement->execute([
            'eventId'       => $eventCallCreateCommand->getEventId(),
            'ip'            => $eventCallCreateCommand->getIp(),
            'lang'          => $eventCallCreateCommand->getLang(),
            'deviceBrand'   => $eventCallCreateCommand->getDeviceBrand(),
            'deviceModel'   => $eventCallCreateCommand->getDeviceModel(),
            'deviceOs'      => $eventCallCreateCommand->getDeviceOs(),
            'clientName'    => $eventCallCreateCommand->getClientName(),
            'clientVersion' => $eventCallCreateCommand->getClientVersion(),
        ]);
        if (!$result) {
            throw new \Exception('Cant create new event call');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int $eventId
     *
     * @return int
     */
    public function deleteByEventId(int $eventId) : int
    {
        $statement = $this->db->prepare('DELETE FROM event_calls WHERE event_id = :eventId');
        $statement->execute([
            'eventId' => $eventId,
        ]);

        return $statement->rowCount();
    }
}
