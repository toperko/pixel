<?php
declare(strict_types=1);
namespace App\Src\EventCalls;

use App\Engine\Common\Collection;

/**
 * Class EventCallModelCollection
 *
 * @package App\Src\EventCalls
 */
class EventCallModelCollection extends Collection
{
    /**
     * EventCallModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(EventCallModel::class);
    }
}
