<?php
declare(strict_types=1);
namespace App\Src\EventCalls;

use App\Engine\Exception\ValidationException;

/**
 * Class EventCallService
 *
 * @package App\Src\EventCalls
 */
class EventCallService
{
    /**
     * @var EventCallCommandRepository
     */
    private $eventCallCommandRepository;

    /**
     * @var EventCallQueryRepository
     */
    private $eventCallQueryRepository;

    /**
     * EventCallService constructor.
     *
     * @param EventCallCommandRepository $eventCallCommandRepository
     * @param EventCallQueryRepository   $eventCallQueryRepository
     */
    public function __construct(
        EventCallCommandRepository $eventCallCommandRepository,
        EventCallQueryRepository $eventCallQueryRepository
    ) {
        $this->eventCallCommandRepository = $eventCallCommandRepository;
        $this->eventCallQueryRepository = $eventCallQueryRepository;
    }

    /**
     * @param EventCallCreateCommand $eventCallCreateCommand
     *
     * @return int
     * @throws ValidationException
     * @throws \Exception
     */
    public function create(EventCallCreateCommand $eventCallCreateCommand) : int
    {
        if (!$eventCallCreateCommand->valid()) {
            throw new ValidationException(
                'Cant create event call ', implode(', ', $eventCallCreateCommand->getErrors())
            );
        }

        return $this->eventCallCommandRepository->create($eventCallCreateCommand);
    }

    /**
     * @param int      $eventId
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return EventCallModelCollection
     * @throws \ReflectionException
     */
    public function getByEventId(int $eventId, ?int $limit = null, ?int $offset = null) : EventCallModelCollection
    {
        return $this->eventCallQueryRepository->getByEventId($eventId, $limit, $offset);
    }

    /**
     * @param int $userId
     *
     * @return EventCallModel|null
     * @throws \Exception
     */
    public function getLastByUserId(int $userId) : ?EventCallModel
    {
        return $this->eventCallQueryRepository->getLastByUserId($userId);
    }

    /**
     * @param int $eventId
     *
     * @return int
     * @throws \Exception
     */
    public function countCallsByEventId(int $eventId) : int
    {
        return $this->eventCallQueryRepository->countCallsByEventId($eventId);
    }

    /**
     * @param int $eventId
     *
     * @return int
     */
    public function deleteByEventId(int $eventId) : int
    {
        return $this->eventCallCommandRepository->deleteByEventId($eventId);
    }
}
