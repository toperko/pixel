<?php
declare(strict_types=1);
namespace App\Src\EventCalls;

use App\Engine\Command;

/**
 * Class EventCallCreateCommand
 *
 * @package App\Src\EventCalls
 */
class EventCallCreateCommand extends Command
{
    /**
     * @var int
     */
    private $eventId;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     */
    private $lang;

    /**
     * @var string
     */
    private $deviceBrand;

    /**
     * @var string
     */
    private $deviceModel;

    /**
     * @var string
     */
    private $deviceOs;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var string
     */
    private $clientVersion;

    /**
     * @return bool
     */
    public function valid() : bool
    {
        return count($this->errors) === 0;
    }

    /**
     * @return int
     */
    public function getEventId() : int
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     *
     * @return EventCallCreateCommand
     */
    public function setEventId(int $eventId) : self
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * @return string
     */
    public function getIp() : string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return EventCallCreateCommand
     */
    public function setIp(string $ip) : self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return string
     */
    public function getLang() : string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     *
     * @return EventCallCreateCommand
     */
    public function setLang(string $lang) : self
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceBrand() : string
    {
        return $this->deviceBrand;
    }

    /**
     * @param string $deviceBrand
     *
     * @return EventCallCreateCommand
     */
    public function setDeviceBrand(string $deviceBrand) : self
    {
        $this->deviceBrand = $deviceBrand;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceModel() : string
    {
        return $this->deviceModel;
    }

    /**
     * @param string $deviceModel
     *
     * @return EventCallCreateCommand
     */
    public function setDeviceModel(string $deviceModel) : self
    {
        $this->deviceModel = $deviceModel;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceOs() : string
    {
        return $this->deviceOs;
    }

    /**
     * @param string $deviceOs
     *
     * @return EventCallCreateCommand
     */
    public function setDeviceOs(string $deviceOs) : self
    {
        $this->deviceOs = $deviceOs;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientName() : string
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName
     *
     * @return EventCallCreateCommand
     */
    public function setClientName(string $clientName) : self
    {
        $this->clientName = $clientName;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientVersion() : string
    {
        return $this->clientVersion;
    }

    /**
     * @param string $clientVersion
     *
     * @return EventCallCreateCommand
     */
    public function setClientVersion(string $clientVersion) : self
    {
        $this->clientVersion = $clientVersion;

        return $this;
    }
}
