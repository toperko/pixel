<?php
declare(strict_types=1);
namespace App\Src\Page;

use App\Engine\Model;

/**
 * Class PageModel
 *
 * @package App\Model
 */
class PageModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $ownerId;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * PageModel constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setId($data['id']);
        $this->setUrl($data['url']);
        $this->setName($data['name']);
        $this->setOwnerId($data['owner_id']);
        $this->setCreatedAt(new \DateTime($data['created_at']));
        $this->setDeletedAt(isset($data['deleted_at']) ? new \DateTime($data['deleted_at']) : null);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return PageModel
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return PageModel
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PageModel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param int $ownerId
     *
     * @return PageModel
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return PageModel
     */
    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt() : ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     *
     * @return PageModel
     */
    public function setDeletedAt(?\DateTime $deletedAt) : self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
