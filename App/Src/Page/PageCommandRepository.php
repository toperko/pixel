<?php
declare(strict_types=1);
namespace App\Src\Page;

/**
 * Class PageCommandRepository
 *
 * @package App\Src\Page
 */
class PageCommandRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * PageCommandRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param PageCreateCommand $pageCreateCommand
     *
     * @return int
     * @throws \Exception
     */
    public function create(PageCreateCommand $pageCreateCommand) : int
    {
        $statement = $this->db->prepare('INSERT INTO pages (
            url,
            name,
            owner_id
        ) VALUES (
            :url,
            :name,
            :owner_id
        )');
        $result = $statement->execute([
            'url'   => $pageCreateCommand->getUrl(),
            'name'  => $pageCreateCommand->getName(),
            'owner_id' => $pageCreateCommand->getOwnerId(),
        ]);
        if (!$result) {
            throw new \Exception('Cant create new page');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $statement = $this->db->prepare('UPDATE pages SET deleted_at = NOW() WHERE id = :id');
        $result = $statement->execute([
            'id'   => $id,
        ]);
        if (!$result) {
            throw new \Exception('Cant delete page');
        }

        return (int) $this->db->lastInsertId();
    }
}
