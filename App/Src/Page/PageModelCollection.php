<?php
declare(strict_types=1);
namespace App\Src\Page;

use App\Engine\Common\Collection;

/**
 * Class PageModelCollection
 *
 * @package App\Src\Page
 * @method PageModel current()
 */
class PageModelCollection extends Collection
{
    /**
     * PageModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(PageModel::class, null, 'id');
    }
}
