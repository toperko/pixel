<?php
declare(strict_types=1);
namespace App\Src\Page;

use App\Engine\Exception\ValidationException;

/**
 * Class PageService
 *
 * @package App\Src\Page
 */
class PageService
{
    /**
     * @var PageCommandRepository
     */
    private $pageCommandRepository;

    /**
     * @var PageQueryRepository
     */
    private $pageQueryRepository;

    /**
     * PageService constructor.
     *
     * @param PageCommandRepository $pageCommandRepository
     * @param PageQueryRepository   $pageQueryRepository
     */
    public function __construct(PageCommandRepository $pageCommandRepository, PageQueryRepository $pageQueryRepository)
    {
        $this->pageCommandRepository = $pageCommandRepository;
        $this->pageQueryRepository = $pageQueryRepository;
    }

    /**
     * @return PageModelCollection
     * @throws \ReflectionException
     */
    public function getAll() : PageModelCollection
    {
        return $this->pageQueryRepository->getAll();
    }

    /**
     * @param int $userId
     *
     * @return PageModelCollection
     * @throws \Exception
     */
    public function getPagesByUserId(int $userId) : PageModelCollection
    {
        return $this->pageQueryRepository->getPagesByUserId($userId);
    }

    /**
     * @param int $id
     *
     * @return PageModel|null
     * @throws \Exception
     */
    public function getById(int $id) : ?PageModel
    {
        return $this->pageQueryRepository->getById($id);
    }

    /**
     * @param PageCreateCommand $pageCreateCommand
     *
     * @return int
     * @throws ValidationException
     * @throws \Exception
     */
    public function create(PageCreateCommand $pageCreateCommand) : int
    {
        if(!$pageCreateCommand->valid()){
            throw new ValidationException('Cant create page', implode(', ', $pageCreateCommand->getErrors()));
        }
        return $this->pageCommandRepository->create($pageCreateCommand);
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $page = $this->getById($id);
        if(!$page){
            throw new \Exception('Cant find this page');
        }
        if($page->getDeletedAt()){
            throw new \Exception('Page is already deleted');
        }

        return $this->pageCommandRepository->delete($id);
    }
}
