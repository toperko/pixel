<?php
declare(strict_types=1);
namespace App\Src\Page;

use App\Engine\Command;

/**
 * Class PageCreateCommand
 *
 * @package App\Src\Page
 */
class PageCreateCommand extends Command
{
    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int
     */
    private $ownerId;

    /**
     * @return bool
     */
    public function valid() : bool
    {
        if (empty($this->name)) {
            $this->errors['name'] = 'Name cant be empty';
        }
        if (!empty($this->name) && strlen($this->name) < 3 || strlen($this->name) > 200) {
            $this->errors['nameLength'] = 'Name must contain from 3 to 200 characters';
        }
        if (empty($this->url)) {
            $this->errors['url'] = 'Address cant be empty';
        }
        if (!empty($this->url) && (strlen($this->url) < 3 || strlen($this->url) > 200)) {
            $this->errors['urlLength'] = 'Address must contain from 3 to 200 characters';
        } elseif (!filter_var($this->url, FILTER_VALIDATE_URL)) {
            $this->errors['url'] = 'Address is not correct';
        }

        return count($this->errors) === 0;
    }

    /**
     * @return string|null
     */
    public function getUrl() : ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return PageCreateCommand
     */
    public function setUrl(?string $url) : self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return PageCreateCommand
     */
    public function setName(?string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getOwnerId() : int
    {
        return $this->ownerId;
    }

    /**
     * @param int $ownerId
     *
     * @return PageCreateCommand
     */
    public function setOwnerId(int $ownerId) : self
    {
        $this->ownerId = $ownerId;

        return $this;
    }
}
