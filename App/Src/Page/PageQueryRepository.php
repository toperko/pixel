<?php
declare(strict_types=1);
namespace App\Src\Page;

/**
 * Class PageQueryRepository
 *
 * @package App\Src\Page
 */
class PageQueryRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * PageQueryRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $pageId
     *
     * @return PageModel|null
     * @throws \Exception
     */
    public function getById(int $pageId) : ?PageModel
    {
        $statement = $this->db->prepare('SELECT
                id,
                url,
                name,
                owner_id,
                created_at,
                deleted_at
            FROM pages WHERE id = :pageId;
        ');
        $result = $statement->execute([
            'pageId' => $pageId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from pages');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new PageModel($result);
    }

    /**
     * @return PageModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getAll() : PageModelCollection
    {
        $userModelCollection = new PageModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                url,
                name,
                owner_id,
                created_at,
                deleted_at
            FROM pages;
        ');
        $result = $statement->execute([]);
        if (!$result) {
            throw new \Exception('Cant get data from pages');
        }
        $result = $statement->fetchAll();

        return $userModelCollection->buildFromArray($result);
    }

    /**
     * @param int $userId
     *
     * @return PageModelCollection
     * @throws \Exception
     */
    public function getPagesByUserId(int $userId) : PageModelCollection
    {
        $userModelCollection = new PageModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                url,
                name,
                owner_id,
                created_at,
                deleted_at
            FROM pages 
            WHERE owner_id = :userId
            AND deleted_at IS NULL;
        ');
        $result = $statement->execute([
            'userId' => $userId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from pages');
        }
        $result = $statement->fetchAll();

        return $userModelCollection->buildFromArray($result);
    }
}
