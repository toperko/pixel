<?php
declare(strict_types=1);
namespace App\Src\User;

use App\Engine\Common\Collection;

/**
 * Class UserModelCollection
 *
 * @package App\Src\User
 *
 * @method UserModel[] get()
 * @method UserModel current()
 */
class UserModelCollection extends Collection
{
    /**
     * UserModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(UserModel::class, null, 'id');
    }
}
