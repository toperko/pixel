<?php
declare(strict_types=1);
namespace App\Src\User;

use App\Engine\Command;

/**
 * Class UserCreateCommand
 *
 * @package App\Domains\User
 */
class UserCreateCommand extends Command
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    private $level;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * @return bool
     */
    public function valid() : bool
    {
        if (!filter_var($this->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $this->errors['emailNotCorrect'] = 'Email is not correct';
        }
        if (!filter_var($this->getPassword(), FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^.{8,}$/']])) {
            $this->errors['password'] = 'Password must contain min 8 charter';
        }

        return count($this->errors) == 0;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return UserCreateCommand
     */
    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return UserCreateCommand
     */
    public function setPassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel() : int
    {
        return $this->level;
    }

    /**
     * @param int $level
     *
     * @return UserCreateCommand
     */
    public function setLevel(int $level) : self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy() : int
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     *
     * @return UserCreateCommand
     */
    public function setCreatedBy(int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
