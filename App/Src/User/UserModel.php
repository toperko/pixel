<?php
declare(strict_types=1);
namespace App\Src\User;

use App\Engine\Model;
use DateTime;

/**
 * Class UserModel
 *
 * @package App\Domains\User
 */
class UserModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    private $level;

    /**
     * @var int|null
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $lastLogin;

    /**
     * UserModel constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setId($data['id']);
        $this->setEmail($data['email']);
        $this->setPassword($data['password']);
        $this->setLevel($data['level']);
        $this->setCreatedBy(isset($data['created_by']) ? $data['created_by'] : null);
        $this->setCreatedAt(new DateTime($data['created_at']));
        $this->setLastLogin(isset($data['last_login']) ? new DateTime($data['last_login']) : null);
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return UserModel
     */
    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return UserModel
     */
    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return UserModel
     */
    public function setPassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserLevelName() : string
    {
        return UserConst::USER_NAMES[$this->getLevel()];
    }

    /**
     * @return int
     */
    public function getLevel() : int
    {
        return $this->level;
    }

    /**
     * @param int $level
     *
     * @return UserModel
     */
    public function setLevel(int $level) : self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCreatedBy() : ?int
    {
        return $this->createdBy;
    }

    /**
     * @param int|null $createdBy
     *
     * @return UserModel
     */
    public function setCreatedBy(?int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt() : DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return UserModel
     */
    public function setCreatedAt(DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLastLogin() : ?DateTime
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTime|null $lastLogin
     *
     * @return UserModel
     */
    public function setLastLogin(?DateTime $lastLogin) : self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }
}
