<?php
declare(strict_types=1);
namespace App\Src\User;

use App\Engine\Session\Session;
use Throwable;
use function time;
use function serialize;
use function unserialize;


/**
 * Class UserSessionService
 *
 * @package App\Src\User
 */
class UserSessionService
{
    /**
     * @var Session;
     */
    private $session;

    /**
     * @const string
     */
    private const USER_SESSION = 'user';

    /**
     * @const string
     */
    private const USER_SESSION_TIME_KEY = 'user_time';

    /**
     * @const string
     */
    private const USER_SESSION_REMEMBER_ME_KEY = 'user_remember_me';

    /**
     * @const int
     */
    private const USER_SESSION_TIME = 60 * 10;

    /**
     * UserSessionService constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @return bool
     */
    public function isUserLogIn() : bool
    {
        return $this->session->has(self::USER_SESSION) && $this->session->has(self::USER_SESSION_TIME_KEY);
    }

    /**
     * @return int
     */
    public function getUserSessionTime() : int
    {
        if (!$this->session->has(self::USER_SESSION_TIME_KEY)) {
            return 0;
        }

        return $this->session->get(self::USER_SESSION_TIME_KEY);
    }

    /**
     * @return UserSessionService
     */
    public function setUserSessionTime() : self
    {
        $this->session->set(self::USER_SESSION_TIME_KEY, time() + self::USER_SESSION_TIME);

        return $this;
    }

    /**
     * @return UserSessionService
     */
    public function setRememberMe() : self
    {
        $this->session->set(self::USER_SESSION_REMEMBER_ME_KEY, true);

        return $this;
    }

    /**
     * @return bool
     */
    public function isRememberMe() : bool
    {
        return $this->session->has(self::USER_SESSION_REMEMBER_ME_KEY);
    }

    /**
     * @return bool
     */
    public function checkUserSessionTime() : bool
    {
        return $this->isRememberMe() || time() < $this->getUserSessionTime();
    }

    /**
     * @param UserModel $userModel
     * @param bool      $rememberMe
     *
     * @return UserSessionService
     */
    public function setUser(UserModel $userModel, bool $rememberMe = false) : self
    {
        $this->session->set(self::USER_SESSION, serialize($userModel));
        $this->setUserSessionTime();
        if ($rememberMe) {
            $this->setRememberMe();
        }

        return $this;
    }

    /**
     * @return UserModel|null
     */
    public function getUser() : ?UserModel
    {
        try {
            return unserialize($this->session->get(self::USER_SESSION));
        } catch (Throwable $exception) {
            return null;
        }
    }

    /**
     * @return UserSessionService
     */
    public function unsetUser() : self
    {
        $this->session->remove(self::USER_SESSION);
        $this->session->remove(self::USER_SESSION_TIME_KEY);
        $this->session->remove(self::USER_SESSION_REMEMBER_ME_KEY);

        return $this;
    }
}
