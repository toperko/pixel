<?php
declare(strict_types=1);
namespace App\Src\User;

use App\Engine\Exception\ValidationException;
use Exception;

/**
 * Class UserService
 *
 * @package App\Domains\User
 */
class UserService
{
    /**
     * @var UserCommandRepository
     */
    private $userCommandRepository;

    /**
     * @var UserQueryRepository
     */
    private $userQueryRepository;

    /**
     * @var UserSessionService
     */
    private $userSessionService;

    /**
     * UserService constructor.
     *
     * @param UserSessionService    $userSessionService
     * @param UserCommandRepository $userCommandRepository
     * @param UserQueryRepository   $userQueryRepository
     */
    public function __construct(
        UserSessionService $userSessionService,
        UserCommandRepository $userCommandRepository,
        UserQueryRepository $userQueryRepository
    ) {
        $this->userSessionService = $userSessionService;
        $this->userCommandRepository = $userCommandRepository;
        $this->userQueryRepository = $userQueryRepository;
    }

    /**
     * @param UserCreateCommand $userCreateCommand
     *
     * @return int
     * @throws Exception
     */
    public function create(UserCreateCommand $userCreateCommand) : int
    {
        if(!$userCreateCommand->valid()){
            throw new ValidationException('Cant create user', implode(', ', $userCreateCommand->getErrors()));
        }
        $userCreateCommand->setPassword(
            $this->hashPassword($userCreateCommand->getPassword())
        );
        if ($this->getUserByEmail($userCreateCommand->getEmail())) {
            throw new Exception('This Email exists');
        }

        return $this->userCommandRepository->create($userCreateCommand);
    }

    /**
     * @return UserModelCollection
     * @throws \ReflectionException
     */
    public function getAll() : UserModelCollection
    {
        return $this->userQueryRepository->getAll();
    }

    /**
     * @param string $email
     *
     * @return UserModel|null
     * @throws \Exception
     */
    public function getUserByEmail(string $email) : ?UserModel
    {
        return $this->userQueryRepository->getUserByEmail($email);
    }

    /**
     * @param string $password
     *
     * @return string
     */
    public function hashPassword(string $password) : string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return UserModel|null
     * @throws \Exception
     */
    public function getUserByEmailAndPassword(string $email, string $password) : ?UserModel
    {
        $user = $this->getUserByEmail($email);
        if ($user && password_verify($password, $user->getPassword())) {
            return $user;
        }

        return null;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return UserModel
     * @throws \Exception
     */
    public function updateUserPassword(string $email, string $password) : UserModel
    {
        $user = $this->getUserByEmail($email);
        if (!$user) {
            throw new \Exception('User does not exist');
        }
        $this->userCommandRepository->updateUserPassword($email, $this->hashPassword($password));

        return $this->getUserByEmail($email);
    }

    /**
     * @param int $userId
     *
     * @return int
     * @throws \Exception
     */
    public function updateLastLogin(int $userId) : int
    {
        return $this->userCommandRepository->updateLastLogin($userId);
    }

    /**
     * @param UserModel $userModel
     * @param bool      $rememberMe
     *
     * @return UserService
     */
    public function userLogin(UserModel $userModel, bool $rememberMe = false) : self
    {
        $this->userSessionService->setUser($userModel, $rememberMe);

        return $this;
    }

    /**
     * @return bool
     */
    public function checkUserIsLogin() : bool
    {
        if ($this->userSessionService->isUserLogIn() && $this->userSessionService->checkUserSessionTime()) {
            $this->userSessionService->setUserSessionTime();

            return true;
        }
        $this->userSessionService->unsetUser();

        return false;
    }

    /**
     * @return UserModel|null
     */
    public function getUserLogin() : ?UserModel
    {
        if (!$this->checkUserIsLogin()) {
            return null;
        }

        return $this->userSessionService->getUser();
    }

    /**
     * @return int
     */
    public function getUserLoginLevel() : int
    {
        if (!$user = $this->getUserLogin()) {
            return 0;
        }

        return $user->getLevel();
    }

    /**
     * @return UserService
     */
    public function logOut() : self
    {
        $this->userSessionService->unsetUser();

        return $this;
    }
}
