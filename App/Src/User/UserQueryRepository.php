<?php
declare(strict_types=1);
namespace App\Src\User;

/**
 * Class UserQueryRepository
 *
 * @package App\Domains\User
 */
class UserQueryRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * UserQueryRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return UserModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getAll() : UserModelCollection
    {
        $userModelCollection = new UserModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                email,
                password,
                level,
                created_by,
                created_at,
                last_login
            FROM users;
        ');
        $result = $statement->execute([]);
        if (!$result) {
            throw new \Exception('Cant get data from users');
        }
        $result = $statement->fetchAll();

        return $userModelCollection->buildFromArray($result);
    }

    /**
     * @param int $userId
     *
     * @return UserModel|null
     * @throws \Exception
     */
    public function getUserById(int $userId) : ?UserModel
    {
        $statement = $this->db->prepare('SELECT
                id,
                email,
                password,
                level,
                created_by,
                created_at,
                last_login
            FROM users
            WHERE id = :id;
        ');
        $result = $statement->execute([
            'id' => $userId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from users');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new UserModel($result);
    }

    /**
     * @param string $email
     *
     * @return UserModel|null
     * @throws \Exception
     */
    public function getUserByEmail(string $email) : ?UserModel
    {
        $statement = $this->db->prepare('SELECT
                id,
                email,
                password,
                level,
                created_by,
                created_at,
                last_login
            FROM users
            WHERE email = :email;
        ');
        $result = $statement->execute([
            'email' => $email,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from users');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new UserModel($result);
    }
}
