<?php
declare(strict_types=1);
namespace App\Engine\Common;

use TypeError;
use Exception;
use stdClass;
use ReflectionClass;
use function class_exists;
use function reset;
use function current;
use function is_bool;
use function key;
use function next;
use function array_key_exists;
use function get_class;
use function ucfirst;
use function strval;
use function call_user_func;
use function end;
use function method_exists;
use function is_object;

/**
 * Class Collection
 *
 * @package App\Engine\Common
 */
class Collection implements \Iterator
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var array
     */
    protected $collection = [];

    /**
     * @var null|string
     */
    protected $interface;

    /**
     * Nazwa domyślnego klucza kolekcji będącego nazwą pola elementu
     *
     * @var null|mixed
     */
    protected $elementKeyName;

    /**
     * Collection constructor.
     *
     * @param string      $type
     * @param null|string $interface
     * @param string|null $elementKeyName
     *
     * @throws TypeError
     */
    public function __construct(string $type, ?string $interface = null, string $elementKeyName = null)
    {
        if (!class_exists($type)) {
            throw new TypeError('Can\'t create collection type ' . $type . '. Class not exist');
        }
        $this->type = $type;
        if (null !== $interface) {
            if (!class_exists($interface)) {
                throw new TypeError('Interface ' . $interface . ' not exist');
            }
            $this->interface = $interface;
        }
        $this->elementKeyName = $elementKeyName;
    }

    /**
     * Implementacja \Iterator
     */
    public function rewind() : void
    {
        reset($this->collection);
    }

    /**
     * Implementacja \Iterator
     *
     * @return null|CollectionInterface
     */
    public function current() : ?CollectionInterface
    {
        $current = current($this->collection);

        return is_bool($current) ? null : $current;
    }

    /**
     * Implementacja \Iterator
     *
     * @return string
     */
    public function key() : string
    {
        return (string) key($this->collection);
    }

    /**
     * Implementacja \Iterator
     */
    public function next() : void
    {
        next($this->collection);
    }

    /**
     * Implementacja \Iterator
     *
     * @return bool
     */
    public function valid() : bool
    {
        return $this->keyExist($this->key());
    }

    /**
     * Pobiera kolekcje
     *
     * @return array
     */
    public function get() : array
    {
        return $this->collection;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function keyExist(string $key) : bool
    {
        return array_key_exists($key, $this->get());
    }

    /**
     * Sprawdzenie czy istnieje ustawiony klucz domyślny elementu
     *
     * @return bool
     */
    protected function isElementKeySet() : bool
    {
        return null !== $this->elementKeyName;
    }

    /**
     * @param CollectionInterface $object
     *
     * @return bool
     * @throws \ReflectionException
     */
    protected function existElementKey(CollectionInterface $object) : bool
    {
        if (!$this->isElementKeySet()) {
            return false;
        }
        if (null === $object) {
            return false;
        }
        if (null !== $object && stdClass::class === get_class($object)) {
            return isset($object->$this->elementKeyName)
                && null !== $object->$this->elementKeyName;
        }
        $reflectionClass = new ReflectionClass($this->type);

        return $reflectionClass->hasMethod('get' . ucfirst($this->elementKeyName));
    }



    /**
     * Pobranie warości klucza domyślnego dla kolekcji
     *
     * @param CollectionInterface $object
     *
     * @return string
     * @throws \ReflectionException
     * @throws Exception
     */
    protected function getElementKeyValue(CollectionInterface $object) : string
    {
        if (!$this->existElementKey($object)) {
            throw new Exception('Typ ustawionego klucza domyślnego dla kolecji nie istnieje w elemencie');
        }
        if (stdClass::class === get_class($object)) {
            return strval($object->$this->elementKeyName);
        }
        $value = call_user_func([$object, 'get' . ucfirst($this->elementKeyName)]);

        return (string) $value;
    }

    /**
     * Dodanie obiektu do kolekcji
     *
     * @param CollectionInterface $object
     * @param null                $key
     *
     * @return bool
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function addToCollection(CollectionInterface $object, $key = null) : bool
    {
        if (!$object instanceof $this->type) {
            throw new TypeError('Can\'t create collection' .
                $this->type . ' with object type ' . get_class($object));
        }
        if (null !== $key) {
            $this->collection[(string) $key] = $object;

            return true;
        }
        if ($this->existElementKey($object)) {
            $this->collection[$this->getElementKeyValue($object)] = $object;

            return true;
        }
        $nextKey = null === $this->getLastElementKey() ? '0' : (string) (((int) $this->getLastElementKey()) + 1);
        $this->collection[$nextKey] = $object;

        return true;
    }

    /**
     * Tworzy kolekcję z tablicy
     *
     * @param array       $elements
     * @param null|string $keyName
     *
     * @return $this
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function buildFromArray(array $elements, ?string $keyName = null) : self
    {
        if (empty($elements)) {
            return $this;
        }
        foreach ($elements as $keyElement => $element) {
            $key = null;
            if ($keyName === 'self') {
                $key = (string) $keyElement;
            } elseif (null !== $keyName && isset($element[$keyName]) && null !== $element[$keyName]) {
                $key = (string) $element[$keyName];
            }
            if (null !== $keyName && $keyName !== 'self') {
                $this->addToCollection(new $this->type($element[$keyName]), $key);
                continue;
            }
            if (is_array($element) || !$element instanceof $this->type) {
                $elementClass = new $this->type($element);
                $this->addToCollection($elementClass, $key);
                continue;
            }
            $this->addToCollection($element, $key);
        }

        return $this;
    }

    /**
     * @return null|string
     */
    private function getLastElementKey() : ?string
    {
        end($this->collection);

        return null === key($this->collection) ? null : (string) key($this->collection);
    }

    /**
     * Sprawdza czy kolekcja jest pusta
     *
     * @return bool
     */
    public function isEmpty() : bool
    {
        return empty($this->collection);
    }

    /**
     * Konwersja kolekcji obiektów do tablicy.
     *
     * @return array
     */
    public function toArray() : array
    {
        $elementsArray = [];
        foreach ($this->get() as $key => $element) {
            if (method_exists($element, 'toArray')) {
                $elementsArray[$key] = $element->toArray();
            } elseif (!is_object($element)) {
                $elementsArray[$key] = $element;
            }
        }

        return $elementsArray;
    }

}
