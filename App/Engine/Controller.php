<?php
declare(strict_types=1);
namespace App\Engine;

use App\Src\User\UserService;
use function filter_var;
use function filter_input;

/**
 * Class Controller
 *
 * @package App\Engine
 */
abstract class Controller
{
    /**
     * @var App
     */
    protected $app;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var Translation
     */
    protected $translation;

    /**
     * @var \PDO
     */
    private $db;

    /**
     * @const string
     */
    public const HTTP_METHOD_GET = 'GET';

    /**
     * @const string
     */
    public const HTTP_METHOD_POST = 'POST';

    /**
     * @const string
     */
    public const HTTP_METHOD_PUT = 'PUT';

    /**
     * @const string
     */
    public const HTTP_METHOD_DELETE = 'DELETE';

    /**
     * Controller constructor.
     *
     * @param App $app
     *
     * @throws Container\ContainerException
     * @throws Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->userService = $app->getUserService();
        $this->db = $app->getContainer()->get('db');
        $this->translation = $app->getContainer()->get('translation');
    }

    /**
     * @param string     $name
     * @param array|null $data
     *
     * @return string
     */
    protected function generateUrl(string $name, ?array $data = null) : string
    {
        if (!$name && !$data) {
            return HTTP_SERVER . '/';
        }
        $collection = $this->app->getRouter()->getCollection();
        $route = $collection->get($name);
        if (isset($route)) {
            return $route->geneRateUrl($data);
        }

        return HTTP_SERVER . '/' . $name;
    }

    /**
     * @param string   $name
     * @param int|null $filter
     *
     * @return null|mixed
     */
    protected function get(string $name, int $filter = null)
    {
        if (!isset($_GET[$name])) {
            return null;
        }
        if ($filter) {
            return filter_var($_GET[$name], $filter);
        }

        return $_GET[$name];
    }

    /**
     * @param string   $name
     * @param int|null $filter
     *
     * @return null|mixed
     */
    protected function post(string $name, int $filter = null)
    {
        if (!isset($_POST[$name])) {
            return null;
        }
        if ($filter) {
            return filter_input(INPUT_POST, $name, $filter);
        }

        return $_POST[$name];
    }

    /**
     * @param string $name
     *
     * @return array|null
     */
    protected function file(string $name) : ?array
    {
        if (!isset($_FILES[$name]) || $_FILES[$name]['size'] === 0) {
            return null;
        }

        return $_FILES[$name];
    }

    /**
     * @return bool
     */
    protected function isPost() : bool
    {
        return $_SERVER['REQUEST_METHOD'] === self::HTTP_METHOD_POST;
    }

    /**
     * @return bool
     */
    protected function isGet() : bool
    {
        return $_SERVER['REQUEST_METHOD'] === self::HTTP_METHOD_GET;
    }

    /**
     * @return bool
     */
    protected function isPut() : bool
    {
        return $_SERVER['REQUEST_METHOD'] === self::HTTP_METHOD_PUT;
    }

    /**
     * @return bool
     */
    protected function isDelete() : bool
    {
        return $_SERVER['REQUEST_METHOD'] === self::HTTP_METHOD_DELETE;
    }

    protected function beginDbTransaction() : void
    {
        if (!$this->db->inTransaction()) {
            $this->db->beginTransaction();
        }
    }

    protected function rollbackDbTransaction() : void
    {
        if ($this->db->inTransaction()) {
            $this->db->rollBack();
        }
    }

    protected function commitDbTransaction() : void
    {
        if ($this->db->inTransaction()) {
            $this->db->commit();
        }
    }

    /**
     * @return View
     * @throws Container\ContainerException
     * @throws Container\ContainerNotFoundException
     * @throws \Throwable
     */
    protected function getView() : View
    {
        $view = $this->app->getContainer()->get('view');
        $view->set('user', $this->userService->getUserLogin());

        return $view;
    }
}
