<?php
declare(strict_types=1);
namespace App\Engine;

use function setcookie;
use function time;

/**
 * Class Cookie
 *
 * @package App\Engine
 */
class Cookie
{
    /**
     * @const int
     */
    public const COOKIE_DEFAULT_TIME = 60 * 60 * 24 * 3;

    /**
     * Cookie constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @param int    $time
     *
     * @return Cookie
     */
    public function set(string $name, $value, int $time = self::COOKIE_DEFAULT_TIME) : self
    {
        setcookie($name, $value, time() + $time, '/');

        return $this;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function has(string $name) : bool
    {
        return isset($_COOKIE[$name]) && !empty($_COOKIE[$name]);
    }

    /**
     * @param string   $name
     * @param int|null $filter
     * @param array    $options
     *
     * @return mixed|null
     */
    public function get(string $name, ?int $filter = null, array $options = [])
    {
        if (!$this->has($name)) {
            return null;
        }
        if ($filter) {
            return filter_input(INPUT_COOKIE, $name, $filter, $options);
        }

        return $_COOKIE[$name];
    }

    /**
     * @param string $name
     *
     * @return Cookie
     */
    public function remove(string $name) : self
    {
        unset($_COOKIE[$name]);
        setcookie($name, '', -1, '/');

        return $this;
    }
}
