<?php
declare(strict_types=1);
namespace App\Engine;

use App\Engine\Common\CollectionInterface;

/**
 * Class Command
 *
 * @package App\Engine
 */
abstract class Command implements CollectionInterface
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @return bool
     */
    public abstract function valid() : bool;

    /**
     * @return array
     */
    public function getErrors() : array
    {
        return $this->errors;
    }
}
