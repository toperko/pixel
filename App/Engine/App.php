<?php
declare(strict_types=1);
namespace App\Engine;

use App\Engine\Container\Container;
use App\Engine\Router\Router;
use App\Engine\Session\Session;
use App\Src\User\UserService;
use Psr\Container\ContainerInterface;
use App\Engine\Container\ContainerException;
use App\Engine\Container\ContainerNotFoundException;
use function is_array;
use function flush;
use function header;

/**
 * Class App
 *
 * @package App\Engine
 */
class App
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Session
     */
    private $session;

    /**
     * App constructor.
     *
     * @param array $container
     */
    public function __construct($container = [])
    {
        if (is_array($container)) {
            $container = new Container($container);
        }
        if (!$container instanceof ContainerInterface) {
            throw new \InvalidArgumentException('Expected a ContainerInterface');
        }
        $this->container = $container;
    }

    /**
     * @throws ContainerException
     * @throws ContainerNotFoundException
     * @throws \Throwable
     */
    public function run()
    {
        $this->router = $this->getContainer()->get('router');
        $this->session = $this->getContainer()->get('session');
        if ($this->router->run()) {
            /**
             * @var $classController Controller
             */
            $classController = 'App\Controller' . $this->router->getClass();
            $method = $this->router->getMethod();
            (new $classController($this))->$method();
        } else {
            $this->redirect($this->getRouter()->generateUrl('404'));
        }
    }

    /**
     * @param string $url
     */
    public function redirect(string $url) : void
    {
        flush();
        header("location: " . $url);
        exit();
    }

    /**
     * @return Container
     */
    public function getContainer() : Container
    {
        return $this->container;
    }

    /**
     * @return Router
     */
    public function getRouter() : Router
    {
        return $this->router;
    }

    /**
     * @return Session
     */
    public function getSession() : Session
    {
        return $this->session;
    }

    /**
     * @return UserService
     * @throws ContainerException
     * @throws ContainerNotFoundException
     * @throws \Throwable
     */
    public function getUserService() : UserService
    {
        return $this->container->get('userService');
    }
}
