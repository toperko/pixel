<?php
declare(strict_types=1);
namespace App\Engine\Container;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class ContainerNotFoundException
 *
 * @package App\Engine
 */
class ContainerNotFoundException extends ContainerException implements NotFoundExceptionInterface
{
}
