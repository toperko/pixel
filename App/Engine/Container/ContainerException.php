<?php
declare(strict_types=1);
namespace App\Engine\Container;

use Psr\Container\ContainerExceptionInterface;

/**
 * Class ContainerException
 *
 * @package App\Engine\Container
 */
class ContainerException extends \Exception implements ContainerExceptionInterface
{
}
