<?php
declare(strict_types=1);
namespace App\Engine\Container;

use Psr\Container\ContainerInterface;
use Pimple\Container as PimpleContainer;

/**
 * Class Container
 *
 * @package App\Engine
 */
class Container extends PimpleContainer implements ContainerInterface
{
    /**
     * Default settings
     *
     * @var array
     */
    private $defaultSettings = [];

    /**
     * Container constructor.
     *
     * @param array $values
     */
    public function __construct(array $values = [])
    {
        parent::__construct($values);
        $userSettings = isset($values['settings']) ? $values['settings'] : [];
        $this->registerDefaultServices($userSettings);
    }

    /**
     * @param $userSettings
     */
    private function registerDefaultServices($userSettings)
    {
        $defaultSettings = $this->defaultSettings;
        $this['settings'] = function() use ($userSettings, $defaultSettings) {
            return new CommonCollection(array_merge($defaultSettings, $userSettings));
        };
    }

    /**
     * @param string $id
     *
     * @return mixed
     * @throws ContainerException
     * @throws ContainerNotFoundException
     * @throws \Throwable
     */
    public function get($id)
    {
        if (!$this->offsetExists($id)) {
            throw new ContainerNotFoundException(sprintf('Identifier "%s" is not defined.', $id));
        }
        try {
            return $this->offsetGet($id);
        } catch (\Throwable $exception) {
            if ($this->exceptionThrownByContainer($exception)) {
                throw new ContainerException(
                    sprintf('Container error while retrieving "%s"', $id),
                    null,
                    $exception
                );
            } else {
                throw $exception;
            }
        }
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function has($id)
    {
        return $this->offsetExists($id);
    }

    /**
     * @param \Throwable $exception
     *
     * @return bool
     */
    private function exceptionThrownByContainer(\Throwable $exception)
    {
        $trace = $exception->getTrace()[0];

        return $trace['class'] === PimpleContainer::class && $trace['function'] === 'offsetGet';
    }
}
