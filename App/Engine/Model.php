<?php
declare(strict_types=1);
namespace App\Engine;

use App\Engine\Common\CollectionInterface;

/**
 * Class Model
 *
 * @package App\Engine
 */
abstract class Model implements CollectionInterface
{}
