<?php
declare(strict_types=1);
namespace App\Engine;

use Exception;
use function substr;
use function scandir;
use function realpath;
use function basename;

/**
 * Class Translation
 *
 * @package App\Engine
 */
class Translation
{
    /**
     * @const string
     */
    private const TRANSLATION_DIR = __DIR__ . '/../../Assets/translations';

    /**
     * @const string
     */
    public const DEFAULT_LANG = 'en';

    /**
     * @const string
     */
    public const COOKIE_LANG_KEY = 'lang';

    /**
     * @var string
     */
    private $lang;

    /**
     * @var array
     */
    private $translations;

    /**
     * @var Cookie
     */
    private $cookie;

    /**
     * Translation constructor.
     *
     * @param Cookie $cookie
     *
     * @throws \Exception
     */
    public function __construct(Cookie $cookie)
    {
        $this->cookie = $cookie;
        $lang = $this->cookie->get(
            self::COOKIE_LANG_KEY,
            FILTER_VALIDATE_REGEXP, [
                'options' => ['regexp' => '/^[a-zA-Z]{2}$/']]
        );
        if ($lang) {
            $this->setLang($lang);

            return;
        }
        $this->choseAutoLang();
    }

    /**
     * @throws \Exception
     */
    public function choseAutoLang() : void
    {
        $langFromRequest = $this->getLangFromRequest();
        foreach ($this->getInstalledLang() as $lang) {
            if ($lang == $langFromRequest) {
                $this->setLang($lang);

                return;
            }
        }
        $this->setLang(self::DEFAULT_LANG);
    }

    /**
     * @param string $lang
     *
     * @throws \Exception
     */
    public function setLang(string $lang) : void
    {
        $this->lang = $lang;
        $this->cookie->set(self::COOKIE_LANG_KEY, $lang);
        if (!$this->getLangFilePath()) {
            $this->setLang(self::DEFAULT_LANG);
            throw new Exception('Cant find ' . $this->lang . '.php in translation dir.');
        }
        $this->translations = require $this->getLangFilePath();
    }

    /**
     * @return null|string
     */
    public function getLangFromRequest() : ?string
    {
        if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            return null;
        }

        return substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    }

    /**
     * @return array
     */
    public function getInstalledLang() : array
    {
        $lang = [];
        $inDir = scandir(realpath(self::TRANSLATION_DIR));
        foreach ($inDir as $file) {
            if ($file == '.' || $file == '..') {
                continue;
            }
            $lang [] = basename($file, '.php');
        }

        return $lang;
    }

    /**
     * @return string
     */
    public function getLang() : string
    {
        return $this->lang;
    }

    /**
     * @return string|null
     */
    public function getLangFilePath() : ?string
    {
        $result = realpath(self::TRANSLATION_DIR . '/' . $this->lang . '.php');

        return $result ? : null;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function has(string $name) : bool
    {
        return isset($this->translations[$name]);
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function __get(string $name) : string
    {
        if (!$this->has($name)) {
            return 'Cant find word: ' . $name . ' in file ' . $this->getLangFilePath();
        }

        return $this->translations[$name];
    }

    /**
     * @param string      $name
     * @param null|string $default
     *
     * @return string
     */
    public function get(string $name, ?string $default = null) : string
    {
        if (!$this->has($name) && !$default) {
            return 'Cant find word: ' . $name . ' in file ' . $this->getLangFilePath();
        }
        if (!$this->has($name)) {
            return $default;
        }

        return $this->translations[$name];
    }
}
