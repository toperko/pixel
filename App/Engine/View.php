<?php
declare(strict_types=1);
namespace App\Engine;

use App\Engine\Router\Router;
use App\Engine\Session\FlashMessage;
use App\Engine\Session\FlashMessageCollection;
use App\Engine\Session\Session;

/**
 *
 * @abstract
 */
class View
{
    /**
     * @var array|object
     */
    protected $settings = [];

    /**
     * @var Translation
     */
    protected $translation;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Cookie
     */
    protected $cookie;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @const string
     */
    public const THEME_COOKIE_KEY = 'theme';

    /**
     * @const string
     */
    public const THEME_LIGHT = 'light';

    /**
     * @const string
     */
    public const THEME_DARK = 'dark';

    /**
     * @var array
     */
    protected $variables = [];

    /**
     * View constructor.
     *
     * @param Translation $translation
     * @param Session     $session
     * @param Cookie      $cookie
     * @param Router      $router
     * @param array       $settings
     */
    public function __construct(
        Translation $translation,
        Session $session,
        Cookie $cookie,
        Router $router,
        array $settings
    ) {
        $this->translation = $translation;
        $this->session = $session;
        $this->cookie = $cookie;
        $this->router = $router;
        $this->setSetting($settings);
        if ($this->cookie->has(self::THEME_COOKIE_KEY) &&
            $this->cookie->get(self::THEME_COOKIE_KEY, FILTER_SANITIZE_STRING) === self::THEME_DARK
        ) {
            $this->addCss('theme-dark', 'styleDark.css');
        }
    }

    /**
     * @param array|null $settings
     *
     * @return View
     */
    public function setSetting(?array $settings = null) : self
    {
        $def = [
            'headerBar' => true,
            'sideMenu'  => true,
            'footer'    => true,
            'bundle'    => '',
            'assets'    => [
                'favicon' => $this->getAssetsUrl('img/favicon/favicon.ico'),
                'css'     => [
                    'bootstrap' => 'bootstrap.min.css',
                    'style'     => 'style.css',
                ],
                'js'      => [
                    'jquery'    => 'jquery-3.3.1.min.js',
                    'bootstrap' => 'bootstrap.min.js',
                    'chart'     => 'Chart.min.js',
                    'popper'    => 'popper.min.js',
                    'feather'   => 'feather.min.js',
                    'script'    => 'script.js',
                ],
            ],
        ];
        if ($settings) {
            $this->settings = json_decode(json_encode($settings + (array) $this->settings + $def));
        } else {
            $this->settings = json_decode(json_encode($def));
        }

        return $this;
    }

    /**
     * @param null|string $name
     * @param array|null  $data
     *
     * @return null|string
     */
    public function generateUrl(?string $name = null, ?array $data = null) : ?string
    {
        if (!$name && !$data) {
            return HTTP_SERVER . '/';
        }
        $collection = $this->router->getCollection();
        $route = $collection->get($name);
        if (isset($route)) {
            return $route->geneRateUrl($data);
        }

        return HTTP_SERVER . '/' . $name;
    }

    /**
     * Wyświetla kod HTML szablonu
     *
     * @param string $name         Nazwa pliku
     * @param string $path         Ścieżka do szablonu
     * @param bool   $renderHeader Wyswietlaj header i footer
     *
     * @return void
     */
    public function renderHTML(string $name, string $path = '', bool $renderHeader = true)
    {
        try {
            if ($renderHeader)
                $this->getHeader();
            if (is_file(DIR_VIEV . $path . $name . '.html.php')) {
                require DIR_VIEV . $path . $name . '.html.php';
            } else {
                throw new \Exception('Cant find: ' . $name . ' in: ' . $path);
            }
            if ($renderHeader)
                $this->getFooter();
        } catch (\Exception $e) {
            echo $e->getMessage() . '<br />
                File: ' . $e->getFile() . '<br />
                Code line: ' . $e->getLine() . '<br />
                Trace: ' . $e->getTraceAsString();
            exit;
        }
    }

    /**
     * @return \stdClass
     */
    public function getCss() : \stdClass
    {
        return $this->settings->assets->css;
    }

    /**
     * @return \stdClass
     */
    public function getJs() : \stdClass
    {
        return $this->settings->assets->js;
    }

    /**
     * @return string
     */
    public function generateCssLinks() : string
    {
        $result = '';
        foreach ($this->getCss() as $name => $file) {
            $result .= '<link href="' . $this->getCssUrl($file) . '" type="text/css"  rel="stylesheet">' . PHP_EOL;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function generateJsLinks() : string
    {
        $result = '';
        foreach ($this->getJs() as $name => $file) {
            $result .= '<script src="' . $this->getJsUrl($file) . '"></script>' . PHP_EOL;
        }

        return $result;
    }

    /**
     * @param string $file
     *
     * @return string
     */
    public function getCssUrl(string $file) : string
    {
        return $this->generateUrl('Assets/css/') . $file;
    }

    /**
     * @param string $file
     *
     * @return string
     */
    public function getJsUrl(string $file) : string
    {
        return $this->generateUrl('Assets/js/') . $file;
    }

    /**
     * @param string $file
     *
     * @return string
     */
    public function getAssetsUrl(string $file) : string
    {
        return $this->generateUrl('Assets/') . $file;
    }

    /**
     * Wyświetla dane JSON.
     *
     * @param array $data Dane do wyświetlenia
     */
    public function renderJSON($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    /**
     * Wyświetla dane JSONP.
     *
     * @param array $data Dane do wyświetlenia
     */
    public function renderJSONP($data)
    {
        header('Content-Type: application/json');
        echo $_GET['callback'] . '(' . json_encode($data) . ')';
        exit();
    }

    /**
     * @param string $name
     * @param string $file
     */
    public function addCss(string $name, string $file) : void
    {
        $this->settings->assets->css->$name = $file;
    }

    /**
     * @param string $name
     * @param string $file
     */
    public function addJs(string $name, string $file) : void
    {
        $this->settings->assets->js->$name = $file;
    }

    /**
     * Ładuje nagłówek strony
     */
    public function getHeader()
    {
        $this->renderHTML('header', 'base/', false);
    }

    /**
     * Ładuje stopkę strony
     */
    public function getFooter()
    {
        $this->renderHTML('footer', 'base/', false);
    }

    /**
     * It sets data.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return View
     */
    public function set($name, $value) : self
    {
        $this->variables[$name] = $value;

        return $this;
    }

    /**
     * @param $name
     * @param $value
     *
     * @return View
     */
    public function __set($name, $value) : self
    {
        $this->variables[$name] = $value;

        return $this;
    }

    /**
     * It gets data.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function get($name)
    {
        if (!isset($this->variables[$name])) {
            return null;
        }
        if (is_string($this->variables[$name])) {
            return htmlspecialchars($this->variables[$name], ENT_QUOTES, 'UTF-8');
        }

        return $this->variables[$name];
    }

    /**
     * It gets data.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (!isset($this->variables[$name])) {
            return null;
        }
        if (is_string($this->variables[$name])) {
            return htmlspecialchars($this->variables[$name], ENT_QUOTES, 'UTF-8');
        }

        return $this->variables[$name];
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function t(string $name) : string
    {
        return $this->translation->$name;
    }

    /**
     * @param FlashMessage $flashMessage
     */
    public function renderFlash(FlashMessage $flashMessage) : void
    {
        echo '<div class="alert alert-' . $flashMessage->getType() . '" role="alert">
                  <h4 class="alert-heading">' . $flashMessage->getTitle() . '</h4>
                  <p>' . $flashMessage->getMessage() . '</p>
                </div>';
    }

    /**
     * @param FlashMessageCollection $flashMessageCollection
     */
    public function renderFlashMessages(FlashMessageCollection $flashMessageCollection) : void
    {
        foreach ($flashMessageCollection as $flashMessage) {
            $this->renderFlash($flashMessage);
        }
    }
}
