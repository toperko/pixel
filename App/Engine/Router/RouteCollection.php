<?php
declare(strict_types=1);
namespace App\Engine\Router;

/**
 * Class RouteCollection
 *
 * @package App\Engine\Router
 */
class RouteCollection
{
    /**
     * @var Route[]
     */
    protected $items;

    /**
     * @var RouteCollection
     */
    private static $instance;

    /**
     * RouteCollection constructor.
     */
    private function __construct() {}

    private function __clone() {}

    /**
     * @return RouteCollection
     */
    public static function getInstance() : RouteCollection
    {
        if (self::$instance === null) {
            self::$instance = new RouteCollection();
        }

        return self::$instance;
    }

    /**
     * Dodaje obiekt Route do kolekcji
     *
     * @param string $name Nazwa elementu
     * @param Route  $item Obiekt Route
     */
    public function add($name, $item) : void
    {
        $this->items[$name] = $item;
    }

    /**
     * @param $name
     *
     * @return Route|null
     */
    public function get($name) : ?Route
    {
        if (array_key_exists($name, $this->items)) {
            return $this->items[$name];
        } else {
            return null;
        }
    }

    /**
     * Zwraca wszystkie obiekty kolekcji
     *
     * @return Route[]
     */
    public function getAll() : array
    {
        return $this->items;
    }
}
