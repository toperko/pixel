<?php
declare(strict_types=1);
namespace App\Engine\Exception;

/**
 * Class ApplicationException
 *
 * @package App\Engine\Exception
 */
class ApplicationException extends \Exception
{
}
