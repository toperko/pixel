<?php
declare(strict_types=1);
namespace App\Engine\Exception;

use Throwable;

/**
 * Class ValidationException
 *
 * @package App\Engine\Exception
 */
class ValidationException extends ApplicationException
{
    protected $errors = [];

    public function __construct(string $message = "", array $errors = [], int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
    }
}
