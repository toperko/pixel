<?php
declare(strict_types=1);
namespace App\Engine\Session;

use App\Engine\Common\CollectionInterface;

/**
 * Class FlashMessage
 *
 * @package App\Engine\Session
 */
class FlashMessage implements CollectionInterface
{
    /**
     * @const string
     */
    public const PRIMARY_TYPE = 'primary';

    /**
     * @const string
     */
    public const SECONDARY_TYPE = 'secondary';

    /**
     * @const string
     */
    public const SUCCESS_TYPE = 'success';

    /**
     * @const string
     */
    public const DANGERS_TYPE = 'danger';

    /**
     * @const string
     */
    public const WARNING_TYPE = 'warning';

    /**
     * @const string
     */
    public const INFO_TYPE = 'info';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $message;

    /**
     * @return string
     */
    public function getType() : string
    {
        if (!isset($this->type)) {
            return self::INFO_TYPE;
        }

        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return FlashMessage
     */
    public function setType(string $type) : self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return FlashMessage
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle() : ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return FlashMessage
     */
    public function setTitle(string $title) : self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return FlashMessage
     */
    public function setMessage(string $message) : self
    {
        $this->message = $message;

        return $this;
    }
}
