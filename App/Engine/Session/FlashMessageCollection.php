<?php
declare(strict_types=1);
namespace App\Engine\Session;

use App\Engine\Common\Collection;

/**
 * Class FlashMessageCollection
 *
 * @package App\Engine\Session
 *
 * @method FlashMessage current()
 */
class FlashMessageCollection extends Collection
{
    /**
     * FlashMessageCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(FlashMessage::class);
    }
}
