<?php
declare(strict_types=1);
namespace App\Engine\Session;

use function serialize;
use function session_start;
use function session_id;
use function session_name;
use function unserialize;

/**
 * Class Session
 *
 * @package App\Engine
 */
class Session
{
    /**
     * @var string
     */
    protected $sessionID;

    /**
     * @const string
     */
    protected const FLASH_MSG_BAG = 'flashMsg';

    /**
     * Session constructor.
     */
    public function __construct()
    {
        if (!isset($_SESSION)) {
            $this->init_session();
        }
        if (!isset($_SESSION[self::FLASH_MSG_BAG])) {
            $_SESSION[self::FLASH_MSG_BAG] = serialize(new FlashMessageCollection());
        }
    }

    /**
     * Start a session
     */
    public function init_session() : void
    {
        session_start();
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return session_id();
    }

    /**
     * @param string $sessionId
     *
     * @return Session
     */
    public function setId(string $sessionId) : self
    {
        session_id($sessionId);

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return session_name();
    }

    /**
     * @param string $name
     *
     * @return Session
     */
    public function setName(string $name) : self
    {
        session_name($name);

        return $this;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function has(string $name) : bool
    {
        return isset($_SESSION[$name]);
    }

    /**
     * @param null|string $name
     *
     * @return bool
     */
    public function isEmpty(?string $name = null) : bool
    {
        if ($name) {
            return empty($_SESSION);
        }

        return empty($_SESSION[$name]);
    }

    /**
     * @param string $name
     * @param        $value
     *
     * @return Session
     */
    public function set(string $name, $value) : self
    {
        $_SESSION[$name] = $value;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get(string $name)
    {
        return $_SESSION[$name];
    }

    /**
     * @return FlashMessageCollection
     */
    public function getFlashCollection() : FlashMessageCollection
    {
        return unserialize($_SESSION[self::FLASH_MSG_BAG]);
    }

    /**
     * @param FlashMessageCollection $flashMessageCollection
     *
     * @return Session
     */
    public function setFlashCollection(FlashMessageCollection $flashMessageCollection) : self
    {
        $_SESSION[self::FLASH_MSG_BAG] = serialize($flashMessageCollection);

        return $this;
    }

    /**
     * @return Session
     */
    public function removeAllFlash() : self
    {
        $this->setFlashCollection(new FlashMessageCollection());

        return $this;
    }

    /**
     * @return bool
     */
    public function hasFlash() : bool
    {
        return !$this->getFlashCollection()->isEmpty();
    }

    /**
     * @param FlashMessage $flashMessage
     * @param null|string  $key
     *
     * @return Session
     * @throws \ReflectionException
     */
    public function addFlash(FlashMessage $flashMessage, ?string $key = null) : self
    {
        $flashMessageCollection = $this->getFlashCollection();
        $flashMessageCollection->addToCollection($flashMessage, $key);
        $this->setFlashCollection($flashMessageCollection);

        return $this;
    }

    /**
     * @param null|string $name
     *
     * @return Session
     */
    public function remove(?string $name = null) : self
    {
        if ($name) {
            unset($_SESSION[$name]);

            return $this;
        }
        unset($_SESSION);

        return $this;
    }
}
